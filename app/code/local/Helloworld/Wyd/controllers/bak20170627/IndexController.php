<?php
/**
 * Created by PhpStorm.
 * User: jacki_hu
 * Date: 2017/5/12
 * Time: 22:08
 */

class Helloworld_Wyd_IndexController extends Mage_Core_Controller_Front_Action
{

    /*
     * 方法说明：获取用户列表
     * 调用方法：http://domain/index.php/account/index/userlist
     */
    public function userlistAction() {
        if (file_exists("/tmp/user_list.csv")) {
            unlink("/tmp/user_list.csv");
        }
        //$client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $session = $client->login('irulu', 'iruluapitest');
        //当前执行脚本的时间
        //$current_time = 1497412800;  //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400;//time();  //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400+86400;  //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400+86400+86400; //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400; //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400+86400;  //20170619  //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400+86400+86400; //20170620 12:01:11 这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400+86400+86400+86400+86400+86400+86400;  //这个时间的用户和订单已经导入
        //$current_time = 1497412800 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400;  //这个时间的用户和订单都已经导入
        //$current_time = 1497412800 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400; //这个时间的用户和订单都已经导入
        //$current_time = 1497412800 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400; //北京时间 2017/6/23 12:0:0 这个时间自动同步数据测试
        date_default_timezone_set("America/Los_Angeles");

        $current_time = time() - 75600; //中午十二点减去21个小时等于太平洋时间的前一天凌晨零点 如26号12点，则减去76500后为太平洋时间的25号零点

        //$current_time = 1498147200;
        $date = $current_time . "==>" . date("Y-m-d H:i:s",$current_time) . "\n";
        $timeHandle = fopen("/tmp/timelog.txt","a+");
        fwrite($timeHandle,$date);
        fclose($timeHandle);
        $current_time_date = date("Y-m-d H:i:s", $current_time);
        //获取24小时前的数据
        $yesterday_time = $current_time - 86400;  //这个时间作为查询时间，以当前时间为基准向前推24小时
        $yesterday_time_date = date("Y-m-d H:i:s", $yesterday_time);

        $complexFilter = array(
            'complex_filter' => array(
                array(
                    'key' => 'created_at',
                    'value' => array('key' => 'gt', 'value' => $yesterday_time_date)  //筛选注册时间大于某个时间点的用户列表
                )
            )
        );

        $user_list = $client->customerCustomerList($session, $complexFilter); //获取用户列表

        foreach ($user_list as $key => $user) {
            $user = (array)$user;
            $order_time = strtotime($user['created_at']);
            if (($order_time <= $current_time) && ($order_time > $yesterday_time)) {  //获取24小时内的注册用户数据
                continue;
            } else {
                unset($user_list[$key]);
            }
        }

        //这里的每个方法都需要判断一下从数据库中是否有获取到数据，如果有才创建文件，如果没有就不要创建，并且不要继续往下执行用户注册，但是订单与用户注册要分开
        //将用户列表写入文件中，因为magento的api获取时间交长，为了防止在操作过程中连接中断造成错误
        $handle = fopen("/tmp/user_list.csv", "w");
        foreach ($user_list as $user) {
            $user = (array)$user;
            $user['created_at'] = strtotime($user['created_at']);
            $user['updated_at'] = strtotime($user['updated_at']);
            if (isset($user['middlename'])) {
                unset($user['middlename']);
            }
            fputcsv($handle, $user);
        }
        fclose($handle);

        $usersHandle = fopen("/tmp/users.txt", "w");
        fwrite($usersHandle, var_export($user_list, true));
        fclose($usersHandle);

        echo 1;
    }

    /*
     * 方法说明：用户地址获取
     * 调用方法：http://domain/index.php/account/index/useraddress
     */
    public function useraddressAction() {
        if(file_exists("/tmp/userinfo.csv")) {
            unlink("/tmp/userinfo.csv");
        }
        if(file_exists("/tmp/user_list.csv")) {
            $client = new SoapClient('https://www.irulu.com/index.php/api/soap/?wsdl');
            $session = $client->login('irulu', 'iruluapitest');

            $handle = fopen("/tmp/user_list.csv","r");

            while(!feof($handle)) {
                $content[] = fgetcsv($handle);
            }
            fclose($handle);

            if(!empty($content)) {
                $userHandle = fopen("/tmp/userinfo.csv","w");

                foreach($content as $k => $user) {
                    if(!empty($user)) {
                        $userinfo[$k]['email'] = $user[6];
                        $userinfo[$k]['lastname'] = $user[8];
                        $userinfo[$k]['firstname'] = $user[7];
                        $userinfo[$k]['password'] = $user[10];
                        $userinfo[$k]['register_date'] = $user[1];
                        $result = $client->call($session, 'customer_address.list', $user[0]); //获取用户地址列表
                        $userinfo[$k]['post_addr'] = $result[0]['street'].','.$result[0]['city'].','.$result[0]['region'].','.$result[0]['postcode'].','.$result[0]['country_id'];
                        $userinfo[$k]['phone'] = $result[0]['telephone'];

                        //将$userinfo数据写入到csv文件中，然后再通过另一个方法读取该文件并将数据通过调用旧网站的api写入旧的数据库
                        fputcsv($userHandle,$userinfo[$k]);
                    }
                }
                fclose($userHandle);
                unlink("/tmp/user_list.csv");
                echo 1;
            }
        }
    }

    /*
     * 方法说明：获取所有订单列表
     *调用方法：http://domain/index.php/account/index/orderlist
     */
    public function orderlistAction() {
        if(file_exists("/tmp/orderid.csv")) {
            unlink("/tmp/orderid.csv");
        }
        $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $session = $client->login('irulu', 'iruluapitest');

        //当前执行脚本的时间
        //$current_time = 1497412800; //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400;//time();  //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400+86400;  //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400+86400+86400;  //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400;  //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400+86400;  //20170619  //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400+86400+86400;  //20170620 12:01:11  这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400+86400+86400+86400+86400+86400+86400;  //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400+86400+86400+86400+86400; //这个时间的用户和订单都已经导入
        //$current_time = 1497412800+86400+86400+86400+86400+86400+86400+86400+86400 + 86400; //这个时间的用户和订单都已经导入
        //$current_time = 1497412800 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400 + 86400;  //北京时间 2017/6/23 12:0:0 这个时间自动同步数据测试
        date_default_timezone_set("America/Los_Angeles");
        $current_time = time() - 75600; //中午十二点减去21个小时等于太平洋时间的前一天凌晨零点 如26号12点，则减去76500后为太平洋时间的25号零点
        $current_time_date = date("Y-m-d H:i:s",$current_time);
        //获取24小时前的数据
        $yesterday_time = $current_time - 86400;  //这个时间作为查询时间，以当前时间为基准向前推24小时
        $yesterday_time_date = date("Y-m-d H:i:s",$yesterday_time);


        $filter = array('complex_filter' => array(array('key' => 'created_at', 'value' => array('key' => 'gt','value'=>$yesterday_time_date))));
        $order_list = $client->salesOrderList($session,$filter);

        if(!empty($order_list)) {
            $orderidHandle = fopen("/tmp/orderid.csv","w");
            $orderSumHandle = fopen("/tmp/order_sum.csv","a+");

            //先通过magento的API筛选出最近24小时的订单，然后再判断订单中已经付款的留下，没有付款的删除
            //在这里筛选
            $orderHandle = fopen("/tmp/orders.txt","w");

            foreach($order_list as $k => $od) {
                $od = (array)$od;
                $order_time = strtotime($od['created_at']);
                if(($order_time <= $current_time) && ($order_time > $yesterday_time)) {
                    continue;
                } else {
                    unset($order_list[$k]);
                }
            }

            foreach($order_list as $key => $order) {
                $order = (array)$order;
                if($order['status'] == 'paid') {
                    continue;
                } else {
                    unset($order_list[$key]);
                }
            }

            fwrite($orderHandle,var_export($order_list,true));


            foreach($order_list as $order) {
                $order = (array)$order;
                $orderid[0]=$order['increment_id'];

                fputcsv($orderidHandle,$orderid);
                fputcsv($orderSumHandle,$orderid);
            }
            fclose($orderidHandle);
            fclose($orderSumHandle);
            fclose($orderHandle);
            echo 1;
        }
    }

    /*
     * 方法说明：获取订单详情
     */
    public function orderdetailAction() {
        if(file_exists("/tmp/orderdetail.csv")) {
            unlink("/tmp/orderdetail.csv");
        }
        $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $session = $client->login('irulu', 'iruluapitest');
        $paytimeHandle = fopen("/tmp/paytime.txt","w");
        $countrys = array (
            'AO' => 'Angola',
            'AF' => 'Afghanistan',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AD' => 'Andorra',
            'AI' => 'Anguilla',
            'AG' => 'Antigua and Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda Is.',
            'BO' => 'Bolivia',
            'BW' => 'Botswana',
            'BR' => 'Brazil',
            'BN' => 'Brunei',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina-faso',
            'MM' => 'Burma',
            'BI' => 'Burundi',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CO' => 'Colombia',
            'CG' => 'Congo',
            'CK' => 'Cook Is.',
            'CR' => 'Costa Rica',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DO' => 'Dominica Rep.',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'EI Salvador',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GD' => 'Grenada',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GN' => 'Guinea',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JO' => 'Jordan',
            'KH' => 'Kampuchea (Cambodia )',
            'KZ' => 'Kazakstan',
            'KE' => 'Kenya',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Laos',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MU' => 'Mauritius',
            'MX' => 'Mexico',
            'MD' => 'Moldova, Republic of',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'MS' => 'Montserrat Is',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'KP' => 'North Korea',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PA' => 'Panama',
            'PG' => 'Papua New Cuinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PL' => 'Poland',
            'PF' => 'French Polynesia',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RO' => 'Romania',
            'RU' => 'Russia',
            'LC' => 'St.Lucia',
            'VC' => 'St.Vincent',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome and Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Is',
            'SO' => 'Somali',
            'ZA' => 'South Africa',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syria',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikstan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TG' => 'Togo',
            'TO' => 'Tonga',
            'TT' => 'Trinidad and Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TW' => 'Taiwan',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VE' => 'Venezuela',
            'VN' => 'Vietnam',
            'YE' => 'Yemen',
            'YU' => 'Yugoslavia',
            'ZW' => 'Zimbabwe',
            'ZR' => 'Zaire',
            'ZM' => 'Zambia',
        );

        $orderidHandle = fopen("/tmp/orderid.csv","r");
        $xxHandle = fopen("/tmp/xx.txt","w");
        $empHandle = fopen("/tmp/empty.txt","w");
        $proHandle = fopen("/tmp/pro.txt","w");
        while(!feof($orderidHandle)) {
            $orderid[] = fgetcsv($orderidHandle);
        }
        fclose($orderidHandle);

        $idHandle = fopen("/tmp/order_id_txt.txt","w");
        fwrite($idHandle,var_export($orderid,true));
        fclose($idHandle);

        if(isset($orderid) && !empty($orderid[0])) {
            $orderDetailHandle = fopen("/tmp/orderdetail.csv","w");
            $orderDetailLogHandle = fopen("/tmp/order_detail_log.txt","w");
            foreach($orderid as $oid) {
                if(!empty($oid)) {
                    //for order
                    $result = $client->salesOrderInfo($session, $oid[0]);
                    $result = (array)$result;
                    $order_detail['order_id'] = $result['increment_id'];
                    $order_detail['create_time'] = strtotime($result['created_at']);
                    $order_detail['email'] = $result['customer_email'];
                    $order_detail['shipping_fee'] = $result['shipping_amount'];
                    $order_detail['total_money'] = $result['base_grand_total'];
                    $order_detail['ip'] = isset($result['remote_ip']) ? sprintf("%u", ip2long($result['remote_ip'])) : "1883904612";  //ip需要转换为数字
                    $order_detail['status'] = $result['status'];
                    $order_detail['update_time'] = strtotime($result['updated_at']);
                    $paymethod = (array)$result['payment'];
                    $order_detail['pay_type'] = $paymethod['method'];
                    $userinfo = (array)$result['shipping_address'];
                    $order_detail['first_name'] = $userinfo['firstname'];
                    $order_detail['last_name'] = $userinfo['lastname'];
                    $order_detail['phone'] = $userinfo['telephone'];
                    $order_detail['zip_code'] = $userinfo['postcode'];
                    $order_detail['abbreviation'] = $userinfo['country_id'];
                    $order_detail['country'] = $countrys[$userinfo['country_id']];  //国家全称
                    $order_detail['state'] = $userinfo['region'];
                    $order_detail['city'] = $userinfo['city'];
                    $order_detail['address1'] = $userinfo['street'].','.$userinfo['city'].','.$userinfo['region'].','.$userinfo['postcode'].','.$order_detail['country'];

                    foreach($result['status_history'] as $paytypes) {
                        $paytypes = (array)$paytypes;
                        if($paytypes['status'] == 'processing') {
                            fwrite($paytimeHandle,$paytypes['created_at']);
                            $order_detail['pay_time'] = strtotime($paytypes['created_at']); //支付时间
                            break;
                        }

                        /*                        foreach($paytypes as $paytype) {
                                                    if($paytype['status'] == 'processing') {
                                                        fwrite($paytimeHandle,$paytype['created_at']);
                                                        $order_detail['pay_time'] = strtotime($paytype['created_at']); //支付时间
                                                        break;
                                                    }
                                                }*/
                    }

                    $products = array();
                    $order_product = array();
                    //for orderdetail
                    foreach($result['items'] as $key => $product) {
                        $product = (array)$product;
                        //将产品剥离出来，具体一个sku下面对应一个产品
                        if($product['product_type'] == "configurable") {
                            $items = array('configurable' => '','simple' => '');
                            $items['configurable'] = $product;
                            $products[$product['sku']] = $items;
                            $items = '';
                            unset($items);
                        } else {
                            $price = $product['price'];
                            $price = (float)$price;
                            $orginal_price = $product['original_price'];
                            $orginal_price = (float)$orginal_price;
                            if(isset($products[$product['sku']]) && empty($price) && empty($orginal_price)) {  // configurable
                                $products[$product['sku']]['simple'] = $product;
                                $products[$product['sku']]['configurable']['name'] = $products[$product['sku']]['simple']['name'];
                                $products[$product['sku']]['configurable']['product_id'] = $products[$product['sku']]['simple']['product_id'];
                                fwrite($empHandle,"config");
                                fwrite($empHandle,"\n");
                                //fwrite($empHandle,var_export($products[$product['sku']]['configurable'],true));
                                //fwrite($empHandle,"\n");
                                $tmp = $products[$product['sku']]['configurable'];
                                $products[$product['sku']]['configurable'] = '';
                                $products[$product['sku']]['simple'] = '';
                                unset($products[$product['sku']]['configurable']);
                                unset($products[$product['sku']]['simple']);
                                //fwrite($empHandle,"tmp");
                                //fwrite($empHandle,"\n");
                                //fwrite($empHandle,var_export($tmp,true));
                                //$products[$product['sku']] = $tmp;
                                $order_product[] = $tmp;
                                $tmp = '';
                                unset($tmp);
                            } else {
                                //$products[$product['sku']] = $product;
                                //fwrite($empHandle,"\n");
                                fwrite($empHandle,"single");
                                fwrite($empHandle,var_export($product,true));
                                $order_product[] = $product;
                            }
                        }

                        foreach($order_product as $pk => $pdt) {
                            $product_item[$pk]['product_id'] = $pdt['product_id'];
                            $product_item[$pk]['order_id'] = $pdt['order_id'];
                            $product_item[$pk]['sku_id'] = 0;
                            $product_item[$pk]['uniqid'] = $pdt['sku'];
                            $qty = ceil($pdt['qty_ordered']) - ceil($pdt['qty_canceled']);
                            $product_item[$pk]['total_money'] = $pdt['price']*$qty;
                            $product_item[$pk]['pay_money'] = $pdt['price']*$qty;
                            $product_item[$pk]['original_price'] = $pdt['original_price'];
                            $product_item[$pk]['price'] = $pdt['price'];
                            $product_item[$pk]['quantity'] = $qty;
                            $product_item[$pk]['product_name'] = $pdt['name'];
                            $product_item[$pk]['product_sku'] = $pdt['sku'];
                            //$product_item[$pk]['product_type'] = $pdt['product_type'];
                        }
                    }
                    $order_detail['items'] = serialize($product_item);
                    $product_item = "";
                    unset($product_item);
                    fputcsv($orderDetailHandle,$order_detail);
                    fwrite($xxHandle,var_export($products,true));
                    fwrite($proHandle,var_export($result,true));
                    $products = '';
                    $order_product = '';
                    unset($order_product);
                    unset($products);
                }/* else {
                $order_detail['items'] = "empty";
                $order_detail['oid'] = $oid[0];

                fputcsv($orderDetailHandle,$order_detail);
            }*/

                //write log
                fwrite($orderDetailLogHandle,$oid);
                fwrite($orderDetailLogHandle,"\n");
                fwrite($orderDetailLogHandle,var_export($order_detail,true));
            }
            fclose($orderDetailHandle);
            fclose($orderDetailLogHandle);
            fclose($proHandle);
            fclose($xxHandle);
            fclose($paytimeHandle);
            fclose($empHandle);
            unlink("/tmp/orderid.csv");
            echo 1;
        }
    }

    /*
     * 方法说明：调用irulu的api进行用户数据的注册
     */
    public function uregisterAction() {

        $header = array(
            'X-App-Device: 10',
            'X-App-Version: 1.2.2',
            'X-Api-Version: v2.0',
            'NO-AUTH: 1',
            'X-UTM: ',
            'X-Uid: 0',
            'X-Did: sdfsfese',
            'X-Token: 55af66b41291512',
            //'X-FORWARDED-FOR: '. $ip,
            //'REMOTE-ADDR: '. $ip,
            //'CLIENT-IP: '. $ip
        );

        $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

        $url_user = "http://47.88.4.2:8082/register2/index?token=55af66b41291512";
        //$url_user ="http://api.account.irulu.com/register2/index?token=55af66b41291512";
        //$url_user ="http://112.74.26.100:10083/register2/index?token=55af66b41291512";
        //$url_order = "http://112.74.26.100:10083/order2/createOrder?token=55af66b41291512";

        $handle = fopen("/tmp/userinfo.csv","r");
        while(!feof($handle)) {
            $users_info[] = fgetcsv($handle);
        }

        $useriHandle = fopen("/tmp/user_info_txt.txt","w");
        fwrite($useriHandle,var_export($users_info,true));
        fclose($useriHandle);

        if(isset($users_info) && !empty($users_info[0])) {
            $registerUserHandle = fopen("/tmp/register_user.csv","a+");
            foreach($users_info as $uk => $user_info) {
                if(!empty($user_info)) {
                    $str_pos = strpos($user_info[3],":");
                    $pwd = substr($user_info[3],0,$str_pos);
                    $salt = substr($user_info[3],$str_pos+1);

                    $user_data =  array(
                        'email'             => $user_info[0],
                        'lastname'            => $user_info[1],
                        'firstname'                => $user_info[2],
                        'password'             => $pwd,//$user_info[3],
                        'salt'             => $salt,//$user_info[3],
                        'register_date'                 => $user_info[4],
                        'last_login_date'              => 1,
                        'user_type'           => '3',
                        'sex'         => 'F',  // 男：M   女：F
                        'post_addr'                => $user_info[5], //邮寄地址
                        'channel'              => 'pcweb',
                        'login_type'              => '1', //用户登录类型
                        'phone'              => $user_info[6],
                    );

                    //调用irulu的api
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url_user);  //1、url
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                    curl_setopt($ch, CURLOPT_POST, 1);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $user_data); //2、data
                    $res = curl_exec($ch);

                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    $register_info[$uk]['uid'] = $res;
                    $register_info[$uk]['email'] = $user_info[0];
                    fputcsv($registerUserHandle,$register_info[$uk]);
                }
            }
            fclose($registerUserHandle);
            fclose($handle);
            unlink("/tmp/userinfo.csv");
            echo 1;
        }
    }


    /*
     * 方法说明：获取magento订单并通过irulu的api保存到数据库
     */
    public function oaddAction() {
        $header = array(
            'X-App-Device: 10',
            'X-App-Version: 1.2.2',
            'X-Api-Version: v2.0',
            'NO-AUTH: 1',
            'X-UTM: ',
            'X-Uid: 0',
            'X-Did: sdfsfese',
            'X-Token: 55af66b41291512',
            //'X-FORWARDED-FOR: '. $ip,
            //'REMOTE-ADDR: '. $ip,
            //'CLIENT-IP: '. $ip
        );

        $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

        //$url_user = "http://api.account.irulu.com/register2/index?token=55af66b41291512";
        $url_order = "http://47.88.4.2/order2/createOrder?token=55af66b41291512";
        $url_order_detail = "http://47.88.4.2/order2/insertOrderDetail?token=55af66b41291512";
        $url_getuid ="http://47.88.4.2:8082/register2/getuid?token=55af66b41291512";

        /*        $url_order = "http://112.74.26.100:10084/order2/createOrder?token=55af66b41291512";
                $url_order_detail = "http://112.74.26.100:10084/order2/insertOrderDetail?token=55af66b41291512";
                $url_getuid ="http://112.74.26.100:10083/register2/getuid?token=55af66b41291512";*/

        $productHandle = fopen("/tmp/product.txt","a+");
        $orderDetailHandle = fopen("/tmp/orderdetail.csv","r");
        $orderLogHandle = fopen("/tmp/order_log.txt","a+");
        $showHandle = fopen("/tmp/show_log.txt","w");
        $uidHandle = fopen("/tmp/uid.csv","w");
        while(!feof($orderDetailHandle)) {
            $orders_info[] = fgetcsv($orderDetailHandle);
        }

        if(isset($orders_info) && !empty($orders_info[0])) {
            foreach($orders_info as $ok => $order_info) {
                if(!empty($order_info)) {
                    //根据email获取用户的uid
                    $email = array('email' => $order_info[2]);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url_getuid);  //1、url
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                    curl_setopt($ch, CURLOPT_POST, 1);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $email); //2、data
                    $uid = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    $u[0] = $uid;
                    fputcsv($uidHandle,$u);
                    if($order_info[8] == "paypal_express") {
                        $pay_type = 1;
                    } else {
                        $pay_type = 2;
                    }
                    $data_order = array(
                        'order_id' => $order_info[0],
                        'uid' => $uid,
                        'mid' => 1,
                        'total_money' => $order_info[4],  //这里需要在购物的时候加个购物券测试是否是折扣后的价格
                        'pay_money' => $order_info[4],
                        'receive_money' => $order_info[4],
                        'shipping_fee' => $order_info[3],
                        'savings_fee' => 0,
                        'insurance_fee' => 0, //商品物流保险费用
                        'first_name' => $order_info[9],
                        'last_name' => $order_info[10],
                        'phone' => $order_info[11],
                        'email' => $order_info[2],
                        'zip_code' => $order_info[12],
                        'abbreviation' => $order_info[13], //国家缩写
                        'country' => $order_info[14],
                        'state' => $order_info[15],
                        'city' => $order_info[16],
                        'address1' => $order_info[17],
                        'address2' => '',
                        'status' => 30, //1 未付款；3 已付款; 30 已付款以确认
                        'device_id' => 'pcweb',
                        'platform' => 0,
                        'order_source' => 10,
                        'channel' => "pcweb",
                        'ip' => !empty($order_info[5]) ? $order_info[5] : "1883904612",  //ip需要转换为数字,
                        'pay_time' => $order_info[18],
                        'create_time' => $order_info[1],
                        'update_time' => $order_info[7],
                        'expired_time' => 0,
                        'pay_type' => $pay_type//$order_info[8]  //1：Paypal、2：信用卡'  支付方式这里需要改为数字
                    );
                    fwrite($showHandle,var_export($data_order,true));
                    //调用irulu的api
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url_order);  //1、url
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                    curl_setopt($ch, CURLOPT_POST, 1);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_order); //2、data
                    $res = curl_exec($ch);

                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    $order_log[$ok]['oid'] = $res;
                    $order_log[$ok]['email'] = $order_info[2];
                    $order_log[$ok]['mess'] = $uid;
                    //fputcsv($orderLogHandle, $order_log[$ok]);
                    fwrite($orderLogHandle,var_export($order_log[$ok],true));
                    fwrite($orderLogHandle,"\n");
                    $order_products = unserialize($order_info[19]);
                    foreach ($order_products as $key => $product) {
                        if (!empty($product)) {
                            $order_detail_data = array(    //一个订单中有多个产品的时候，需要多个这样的数据结构
                                'order_id' => $data_order['order_id'],
                                'uid' => $uid,  //这里需要填写uid
                                'parent_id' => 0,
                                'product_id' => $product['product_id'],
                                'sku_id' => 0,
                                // 'total_money'           => $product['originalPrice'] * $product['quantity'],
                                'total_money' => $product['total_money'],
                                'pay_money' => $product['pay_money'],
                                'original_price' => $product['original_price'],
                                'price' => $product['price'],
                                'quantity' => $product['quantity'],
                                'product_name' => $product['product_name'],
                                'product_sku' => '',
                                'product_image' => '',
                                'uniqid' => $product['product_sku'],  //sku
                                'promotion_product_id' => 0
                            );

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url_order_detail);  //1、url
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                            curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                            curl_setopt($ch, CURLOPT_POST, 1);

                            curl_setopt($ch, CURLOPT_POSTFIELDS, $order_detail_data); //2、data
                            $res = curl_exec($ch);

                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            curl_close($ch);
                            $order_log[$ok]['did'] = $res;
                            $order_log[$ok]['email'] = $order_info[2];
                            $order_log[$ok]['product'] = $order_detail_data;
                            $order_log[$ok]['uid'] = $uid;
                            //fputcsv($productHandle, $order_log[$ok]);
                            fwrite($productHandle,var_export($order_log[$ok],true));
                            fwrite($productHandle,"\n");
                        }
                    }
                }/* else {
                    $order_log[$ok]['did'] = $res;
                    $order_log[$ok]['order'] = $data_order['order_id'];
                    $order_log[$ok]['res'] = "empty";
                    fwrite($productHandle,var_export($order_log[$ok]));
                    fwrite($productHandle,"\n");
                }*/
            }
            fclose($productHandle);
            fclose($orderDetailHandle);
            fclose($orderLogHandle);
            fclose($showHandle);
            fclose($uidHandle);
            unlink("/tmp/orderdetail.csv");
            echo 1;
        }
    }

    /*    public function getuidAction() {
            $url_user ="http://112.74.26.100:10083/register2/getuid?token=55af66b41291512";
            $header = array(
                'X-App-Device: 10',
                'X-App-Version: 1.2.2',
                'X-Api-Version: v2.0',
                'NO-AUTH: 1',
                'X-UTM: ',
                'X-Uid: 0',
                'X-Did: sdfsfese',
                'X-Token: 55af66b41291512',
                //'X-FORWARDED-FOR: '. $ip,
                //'REMOTE-ADDR: '. $ip,
                //'CLIENT-IP: '. $ip
            );

            $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

            $email = array('email' => "cren3@163.com");;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url_user);  //1、url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

            curl_setopt($ch, CURLOPT_POST, 1);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $email); //2、data
            $res = curl_exec($ch);

            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            echo "<pre>";
            print_r($res);
        }*/

    public function getproductAction() {
        $handle = fopen("/tmp/orderdetail.csv","r");
        while(!feof($handle)) {
            $orders[] = fgetcsv($handle);
        }
        fclose($handle);
        $file = fopen("/tmp/order_count.txt","w");
        $pro = fopen("/tmp/product_count,txt","w");
        fwrite($file,var_export($orders,true));

        foreach($orders as $order) {
            $products = unserialize($order[18]);
            fwrite($pro,var_export($products,true));
        }

        fclose($file);
        fclose($pro);
    }

    public function statusAction() {
        $a = '0.0000';
        $a = (float)$a;
        echo $a;/*
        if(empty($a)) {
            echo "empty:" . $a;
        } else {
            echo "not empty:" . $a;
        }*/
    }
}