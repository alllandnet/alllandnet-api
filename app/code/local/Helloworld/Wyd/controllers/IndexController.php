<?php
/**
 * Created by PhpStorm.
 * User: jacki_hu
 * Date: 2017/5/12
 * Time: 22:08
 */

class Helloworld_Wyd_IndexController extends Mage_Core_Controller_Front_Action
{

    /*
     * 方法说明：获取用户列表  中午十二点执行
     * 调用方法：http://domain/index.php/account/index/userlist
     */
    public function userlistAction() {
        if (file_exists("/tmp/users/user_list.csv")) {
            unlink("/tmp/users/user_list.csv");
        }
        //$client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $session = $client->login('irulu', 'iruluapitest');
        //当前执行脚本的时间
        date_default_timezone_set("America/Los_Angeles");

        //$current_time = time() - 75600; //中午十二点减去21个小时等于太平洋时间的前一天凌晨零点 如26号12点，则减去76500后为太平洋时间的25号零点
        $current_time = time(); //中午十二点减去21个小时等于太平洋时间的前一天凌晨零点 如26号12点，则减去76500后为太平洋时间的25号零点
        //$current_time = 1509120000; //中午十二点减去21个小时等于太平洋时间的前一天凌晨零点 如26号12点，则减去76500后为太平洋时间的25号零点

        //$current_time = 1498147200;
        $date = $current_time . "==>" . date("Y-m-d H:i:s",$current_time) . "\n";
        $timeHandle = fopen("/tmp/users/timelog.txt","a+");
        fwrite($timeHandle,$date);
        fclose($timeHandle);
        $current_time_date = date("Y-m-d H:i:s", $current_time);
        //获取24小时前的数据
        //$yesterday_time = $current_time - 140400;  //这个时间作为查询时间，以当前时间为基准向前推24小时
        //$yesterday_time = $current_time - 86400;  //这个时间作为查询时间，以当前时间为基准向前推24小时
        //$yesterday_time = $current_time - 43200;  //每隔12个小时执行一次获取用户注册信息,在晚上6点将今天12个小时的用户先导进erp，然后将时间修改为每隔6个小时执行一次
        //$yesterday_time = $current_time - 10800;  //每隔3个小时
        $yesterday_time = $current_time - 21600; //隔6个小时
        //$yesterday_time = $current_time - 33600; //隔9个小时
        //$yesterday_time = $current_time - 64800; //隔18个小时
        //$yesterday_time = $current_time - 86400; //隔24个小时
        //$yesterday_time = $current_time - 259200; //隔72个小时
        $yesterday_time_date = date("Y-m-d H:i:s", $yesterday_time);

        $complexFilter = array(
            'complex_filter' => array(
                array(
                    'key' => 'created_at',
                    'value' => array('key' => 'gt', 'value' => $yesterday_time_date)  //筛选注册时间大于某个时间点的用户列表
                )
            )
        );

        $user_list = $client->customerCustomerList($session, $complexFilter); //获取用户列表

        foreach ($user_list as $key => $user) {
            $user = (array)$user;
            $order_time = strtotime($user['created_at']);
            if (($order_time <= $current_time) && ($order_time > $yesterday_time)) {  //获取24小时内的注册用户数据
                continue;
            } else {
                unset($user_list[$key]);
            }
        }

        //这里的每个方法都需要判断一下从数据库中是否有获取到数据，如果有才创建文件，如果没有就不要创建，并且不要继续往下执行用户注册，但是订单与用户注册要分开
        //将用户列表写入文件中，因为magento的api获取时间交长，为了防止在操作过程中连接中断造成错误
        $handle = fopen("/tmp/users/user_list.csv", "w");
        foreach ($user_list as $user) {
            $user = (array)$user;
            $user['created_at'] = strtotime($user['created_at']);
            $user['updated_at'] = strtotime($user['updated_at']);
            if (isset($user['middlename'])) {
                unset($user['middlename']);
            }
            fputcsv($handle, $user);
        }
        fclose($handle);

        $usersHandle = fopen("/tmp/users/users.txt", "w");
        fwrite($usersHandle, var_export($user_list, true));
        fclose($usersHandle);

        echo 1;
    }

    /*
     * 方法说明：用户地址获取  中午12点的时候执行
     * 调用方法：http://domain/index.php/account/index/useraddress
     */
    public function useraddressAction() {
        if(file_exists("/tmp/users/userinfo.csv")) {
            unlink("/tmp/userinfo.csv");
        }
        if(file_exists("/tmp/users/user_list.csv")) {
            $client = new SoapClient('https://www.irulu.com/index.php/api/soap/?wsdl');
            $session = $client->login('irulu', 'iruluapitest');

            $handle = fopen("/tmp/users/user_list.csv","r");

            while(!feof($handle)) {
                $content[] = fgetcsv($handle);
            }
            fclose($handle);

            if(!empty($content)) {
                $userHandle = fopen("/tmp/users/userinfo.csv","w");

                foreach($content as $k => $user) {
                    if(!empty($user)) {
                        $userinfo[$k]['email'] = $user[6];
                        $userinfo[$k]['lastname'] = $user[8];
                        $userinfo[$k]['firstname'] = $user[7];
                        $userinfo[$k]['password'] = $user[10];
                        $userinfo[$k]['register_date'] = $user[1];
                        $result = $client->call($session, 'customer_address.list', $user[0]); //获取用户地址列表
                        $userinfo[$k]['post_addr'] = $result[0]['street'].','.$result[0]['city'].','.$result[0]['region'].','.$result[0]['postcode'].','.$result[0]['country_id'];
                        $userinfo[$k]['phone'] = $result[0]['telephone'];

                        //将$userinfo数据写入到csv文件中，然后再通过另一个方法读取该文件并将数据通过调用旧网站的api写入旧的数据库
                        fputcsv($userHandle,$userinfo[$k]);
                    }
                }
                fclose($userHandle);
                unlink("/tmp/users/user_list.csv");
                echo 1;
            }
        }
    }

    /*
     * 方法说明：获取所有订单列表（此方法放弃）
     *调用方法：http://domain/index.php/account/index/orderlist
     */
    public function orderlistAction() {
        if(file_exists("/tmp/orderid.csv")) {
            unlink("/tmp/orderid.csv");
        }
        $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $session = $client->login('irulu', 'iruluapitest');

        //当前执行脚本的时间
        date_default_timezone_set("America/Los_Angeles");
        $current_time = time() - 75600; //中午十二点减去21个小时等于太平洋时间的前一天凌晨零点 如26号12点，则减去76500后为太平洋时间的25号零点
        $current_time_date = date("Y-m-d H:i:s",$current_time);
        //获取24小时前的数据
        $yesterday_time = $current_time - 86400;  //这个时间作为查询时间，以当前时间为基准向前推24小时
        $yesterday_time_date = date("Y-m-d H:i:s",$yesterday_time);


        $filter = array('complex_filter' => array(array('key' => 'created_at', 'value' => array('key' => 'gt','value'=>$yesterday_time_date))));
        $order_list = $client->salesOrderList($session,$filter);

        if(!empty($order_list)) {
            $orderidHandle = fopen("/tmp/orderid.csv","w");
            $orderSumHandle = fopen("/tmp/order_sum.csv","a+");

            //先通过magento的API筛选出最近24小时的订单，然后再判断订单中已经付款的留下，没有付款的删除
            //在这里筛选
            $orderHandle = fopen("/tmp/orders.txt","w");

            foreach($order_list as $k => $od) {
                $od = (array)$od;
                $order_time = strtotime($od['created_at']);
                if(($order_time <= $current_time) && ($order_time > $yesterday_time)) {
                    continue;
                } else {
                    unset($order_list[$k]);
                }
            }

            foreach($order_list as $key => $order) {
                $order = (array)$order;
                if($order['status'] == 'paid') {
                    continue;
                } else {
                    unset($order_list[$key]);
                }
            }

            fwrite($orderHandle,var_export($order_list,true));


            foreach($order_list as $order) {
                $order = (array)$order;
                $orderid[0]=$order['increment_id'];

                fputcsv($orderidHandle,$orderid);
                fputcsv($orderSumHandle,$orderid);
            }
            fclose($orderidHandle);
            fclose($orderSumHandle);
            fclose($orderHandle);
            echo 1;
        }
    }

    /*
     * 方法说明：获取所有订单列表（获取更新状态为某个时间的订单）用这个方法替代上面的 orderlistAction 方法，每天下午3点执行此方法，则订单详情的也要在下午3点后执行
     *调用方法：http://domain/index.php/account/index/orderlist
     */
    public function orderlistbyupdatetimeAction() {
        date_default_timezone_set("Asia/Shanghai");
        $weekday = date("w",time());
        switch($weekday) {
            case 0: $action = 0;break;
            case 6: $action = 1;break;
            default:$action = 1;break;
        }

        if($action) {
            if(file_exists("/tmp/paidorder/orderid.csv")) {
                unlink("/tmp/paidorder/orderid.csv");
            }
            $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
            $session = $client->login('irulu', 'iruluapitest');

            //当前执行脚本的时间
            date_default_timezone_set("America/Los_Angeles");
            $current_time = time();
            $current_time_date = date("Y-m-d H:i:s",$current_time);
            //获取24小时前的数据
            $yesterday_time = $current_time - 32400;  //这个时间作为查询时间，查询每天上午六点后状态更新为paid的订单，因为订单修改为paid状态是统一某个时段，所以只要找出该时间范围就行
            $yesterday_time_date = date("Y-m-d H:i:s",$yesterday_time);

            $filter = array(
                'filter' => array(   //固定筛选某个
                    array('key' => 'status', 'value' => 'paid'),
                ),
                'complex_filter' => array(
                    array(
                        'key' => 'updated_at',
                        'value' => array(
                            'key' => 'gt',
                            'value' => $yesterday_time_date
                        ),
                    ),
                )
            );
            $order_list = $client->salesOrderList($session,$filter);

            if(!empty($order_list)) {
                $orderidHandle = fopen("/tmp/paidorder/orderid.csv","w");
                $orderSumHandle = fopen("/tmp/paidorder/order_sum.csv","a+");

                //先通过magento的API筛选出最近24小时的订单，然后再判断订单中已经付款的留下，没有付款的删除
                //在这里筛选
                $orderHandle = fopen("/tmp/paidorder/orders.txt","w");

                fwrite($orderHandle,var_export($order_list,true));


                foreach($order_list as $order) {
                    if(!empty($order)) {
                        $order = (array)$order;
                        $orderid[0]=$order['increment_id'];

                        fputcsv($orderidHandle,$orderid);
                        fputcsv($orderSumHandle,$orderid);
                    }
                }
                fclose($orderidHandle);
                fclose($orderSumHandle);
                fclose($orderHandle);
                echo 1;
            }
        }
    }

    //修改订单状态  此方法必须手动执行，不能自动执行  工作日（1-5）下午3点前执行
    public function changestatusAction() {
        if(file_exists("/tmp/paidorder/orderpay.csv")) {
            $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
            $session = $client->login('irulu', 'iruluapitest');
            $orderpayHandle = fopen("/tmp/paidorder/orderpay.csv","r");
            while(!feof($orderpayHandle)) {
                $orders[] = fgetcsv($orderpayHandle);
            }
            fclose($orderpayHandle);
            $statusHandle = fopen("/tmp/paidorder/change_status.txt","a+");
            foreach($orders as $k => $o) {
                if(!empty($o)) {
                    $result = $client->salesOrderAddComment($session,$o[0],"paid");
                    fwrite($statusHandle,$o[0]." -- ");
                    fwrite($statusHandle,$result);
                    fwrite($statusHandle,"\n");
                }
            }
            unlink("/tmp/paidorder/orderpay.csv");
            unlink("/tmp/processingorders/orderpay.csv");
            fclose($statusHandle);
        }
    }


    //将付款未确认的订单列表导出  每天早上六点执行，星期六星期天不执行
    public function payorderlistAction() {
        date_default_timezone_set("Asia/Shanghai");
        $weekday = date("w",time());
        switch($weekday) {
            case 0: $action = 0;break;
            case 6: $action = 1;break;
            default:$action = 1;break;
        }

        if($action) {
            if(file_exists("/tmp/processingorders/processing_orderid.csv")) {
                unlink("/tmp/processingorders/processing_orderid.csv");
            }
            $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
            $session = $client->login('irulu', 'iruluapitest');

            $filter = array(
                'filter' => array(   //固定筛选某个
                    array('key' => 'status', 'value' => 'processing'),
                ),
                'complex_filter' => array(
                    array(
                        'key' => 'updated_at',
                        'value' => array(
                            'key' => 'gt',
                            'value' => "2017-06-26 00:00:00"
                        ),
                    ),
                )
            );
            $order_list = $client->salesOrderList($session,$filter);
            $orderHandle = fopen("/tmp/processingorders/processing_orders.txt","w");
            fwrite($orderHandle,var_export($order_list,true));
            fclose($orderHandle);
            $current_time = time() - 86400 - 54000; //中午十二点减去21个小时等于太平洋时间的前一天凌晨零点 如26号6点，则减去54000后为太平洋时间的25号零点;
            $time = time();//文件后缀
            if(!empty($order_list)) {
                $orderidHandle = fopen("/tmp/processingorders/processing_orderid.csv", "w");
                //用于备份，如果这天的订单被导出但是忘记确认那么就使用这个备份重新确认，因为processing_orderid.csv在每天的更新中会被覆盖
                $orderidbakHandle = fopen("/tmp/processingorders/processing_orderid-".$time.".csv", "w");
                foreach($order_list as $k => $order) {
                    $order = (array)$order;
                    $order_time = strtotime($order['created_at']);
                    if($order_time <= $current_time) {
                        continue;
                    } else {
                        unset($order_list[$k]);
                    }
                }

                foreach($order_list as $ok => $or) {
                    $or = (array)$or;
                    $orderid[0]=$or['increment_id'];
                    fputcsv($orderidbakHandle,$orderid);
                    fputcsv($orderidHandle,$orderid);
                }
                fclose($orderidHandle);
                fclose($orderidbakHandle);
            }
        }
    }


    //导出订单用于确认付款，paypal和钱海  周六周日不执行
    public function getorderdetailAction() {
        date_default_timezone_set("Asia/Shanghai");
        $weekday = date("w",time());

        switch($weekday) {
            case 0: $action = 0;break;
            case 6: $action = 1;break;
            default:$action = 1;break;
        }
        if($action) {
            if(file_exists("/tmp/processingorders/processing_orderid.csv")) {
                $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
                $session = $client->login('irulu', 'iruluapitest');
                $orderid2Handle = fopen("/tmp/processingorders/processing_orderid.csv","r");
                while(!feof($orderid2Handle)) {
                    $orderid[] = fgetcsv($orderid2Handle);
                }
                fclose($orderid2Handle);
                $ordersinfo = fopen("/tmp/processingorders/processing_orders_info.txt","w");
                $orderpay = fopen("/tmp/processingorders/orderpay.csv","w");
                if(isset($orderid) && !empty($orderid[0])) {
                    foreach($orderid as $oid) {
                        if(!empty($oid)) {
                            $result = $client->salesOrderInfo($session, $oid[0]);
                            fwrite($ordersinfo,var_export($result,true));
                            $result = (array)$result;
                            $paymethod = (array)$result['payment'];
                            if($paymethod['method'] == 'paypal_express') {
                                $d['order_id'] = $result['increment_id'];
                                $d['pay_method'] = $paymethod['method'];
                                $trans = $result['status_history'];
                                foreach($trans as $k => $ts) {
                                    $ts = (array)$ts;
                                    if($ts['status'] == 'processing') {
                                        $d['transaction_id'] = strip_tags($ts['comment']);
                                        break;
                                    }
                                }
                            } else {
                                $d['order_id'] = $result['increment_id'];
                                $d['pay_method'] = $paymethod['method'];
                                $d['transaction_id'] = '';
                            }
                            fputcsv($orderpay,$d);
                            //fwrite($ordersinfo,$oid[0]);
                            //fwrite($ordersinfo,"\n");
                        }
                    }
                    unlink("/tmp/processingorders/processing_orderid.csv");
                }

                fclose($orderpay);
                fclose($ordersinfo);
            }
        }
    }

    /*
     * 方法说明：获取订单详情  这个方法要放到3点以后执行，因为其需要orderid.csv这个文件   周六周日不执行
     */
    public function orderdetailAction() {
        date_default_timezone_set("Asia/Shanghai");
        $weekday = date("w",time());

        switch($weekday) {
            case 0: $action = 0;break;
            case 6: $action = 1;break;
            default:$action = 1;break;
        }

        if($action) {
            if(file_exists("/tmp/paidorder/orderdetail.csv")) {
                unlink("/tmp/paidorder/orderdetail.csv");
            }

            if(file_exists("/tmp/paidorder/orderid.csv")) {
                $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
                $session = $client->login('irulu', 'iruluapitest');
                $paytimeHandle = fopen("/tmp/paidorder/paytime.txt","w");
                $countrys = array (
                    'AO' => 'Angola',
                    'AF' => 'Afghanistan',
                    'AL' => 'Albania',
                    'DZ' => 'Algeria',
                    'AD' => 'Andorra',
                    'AI' => 'Anguilla',
                    'AG' => 'Antigua and Barbuda',
                    'AR' => 'Argentina',
                    'AM' => 'Armenia',
                    'AU' => 'Australia',
                    'AT' => 'Austria',
                    'AZ' => 'Azerbaijan',
                    'BS' => 'Bahamas',
                    'BH' => 'Bahrain',
                    'BD' => 'Bangladesh',
                    'BB' => 'Barbados',
                    'BY' => 'Belarus',
                    'BE' => 'Belgium',
                    'BZ' => 'Belize',
                    'BJ' => 'Benin',
                    'BM' => 'Bermuda Is.',
                    'BO' => 'Bolivia',
                    'BW' => 'Botswana',
                    'BR' => 'Brazil',
                    'BN' => 'Brunei',
                    'BG' => 'Bulgaria',
                    'BF' => 'Burkina-faso',
                    'MM' => 'Burma',
                    'BI' => 'Burundi',
                    'CM' => 'Cameroon',
                    'CA' => 'Canada',
                    'CF' => 'Central African Republic',
                    'TD' => 'Chad',
                    'CL' => 'Chile',
                    'CN' => 'China',
                    'CO' => 'Colombia',
                    'CG' => 'Congo',
                    'CK' => 'Cook Is.',
                    'CR' => 'Costa Rica',
                    'CU' => 'Cuba',
                    'CY' => 'Cyprus',
                    'CZ' => 'Czech Republic',
                    'DK' => 'Denmark',
                    'DJ' => 'Djibouti',
                    'DO' => 'Dominica Rep.',
                    'EC' => 'Ecuador',
                    'EG' => 'Egypt',
                    'SV' => 'EI Salvador',
                    'EE' => 'Estonia',
                    'ET' => 'Ethiopia',
                    'FJ' => 'Fiji',
                    'FI' => 'Finland',
                    'FR' => 'France',
                    'GF' => 'French Guiana',
                    'GA' => 'Gabon',
                    'GM' => 'Gambia',
                    'GE' => 'Georgia',
                    'DE' => 'Germany',
                    'GH' => 'Ghana',
                    'GI' => 'Gibraltar',
                    'GR' => 'Greece',
                    'GD' => 'Grenada',
                    'GU' => 'Guam',
                    'GT' => 'Guatemala',
                    'GN' => 'Guinea',
                    'GY' => 'Guyana',
                    'HT' => 'Haiti',
                    'HN' => 'Honduras',
                    'HK' => 'Hong Kong',
                    'HU' => 'Hungary',
                    'IS' => 'Iceland',
                    'IN' => 'India',
                    'ID' => 'Indonesia',
                    'IR' => 'Iran',
                    'IQ' => 'Iraq',
                    'IE' => 'Ireland',
                    'IL' => 'Israel',
                    'IT' => 'Italy',
                    'JM' => 'Jamaica',
                    'JP' => 'Japan',
                    'JO' => 'Jordan',
                    'KH' => 'Kampuchea (Cambodia )',
                    'KZ' => 'Kazakstan',
                    'KE' => 'Kenya',
                    'KR' => 'Korea',
                    'KW' => 'Kuwait',
                    'KG' => 'Kyrgyzstan',
                    'LA' => 'Laos',
                    'LV' => 'Latvia',
                    'LB' => 'Lebanon',
                    'LS' => 'Lesotho',
                    'LR' => 'Liberia',
                    'LY' => 'Libya',
                    'LI' => 'Liechtenstein',
                    'LT' => 'Lithuania',
                    'LU' => 'Luxembourg',
                    'MO' => 'Macao',
                    'MG' => 'Madagascar',
                    'MW' => 'Malawi',
                    'MY' => 'Malaysia',
                    'MV' => 'Maldives',
                    'ML' => 'Mali',
                    'MT' => 'Malta',
                    'MU' => 'Mauritius',
                    'MX' => 'Mexico',
                    'MD' => 'Moldova, Republic of',
                    'MC' => 'Monaco',
                    'MN' => 'Mongolia',
                    'MS' => 'Montserrat Is',
                    'MA' => 'Morocco',
                    'MZ' => 'Mozambique',
                    'NA' => 'Namibia',
                    'NR' => 'Nauru',
                    'NP' => 'Nepal',
                    'NL' => 'Netherlands',
                    'NZ' => 'New Zealand',
                    'NI' => 'Nicaragua',
                    'NE' => 'Niger',
                    'NG' => 'Nigeria',
                    'KP' => 'North Korea',
                    'NO' => 'Norway',
                    'OM' => 'Oman',
                    'PK' => 'Pakistan',
                    'PA' => 'Panama',
                    'PG' => 'Papua New Cuinea',
                    'PY' => 'Paraguay',
                    'PE' => 'Peru',
                    'PH' => 'Philippines',
                    'PL' => 'Poland',
                    'PF' => 'French Polynesia',
                    'PT' => 'Portugal',
                    'PR' => 'Puerto Rico',
                    'QA' => 'Qatar',
                    'RO' => 'Romania',
                    'RU' => 'Russia',
                    'LC' => 'St.Lucia',
                    'VC' => 'St.Vincent',
                    'SM' => 'San Marino',
                    'ST' => 'Sao Tome and Principe',
                    'SA' => 'Saudi Arabia',
                    'SN' => 'Senegal',
                    'SC' => 'Seychelles',
                    'SL' => 'Sierra Leone',
                    'SG' => 'Singapore',
                    'SK' => 'Slovakia',
                    'SI' => 'Slovenia',
                    'SB' => 'Solomon Is',
                    'SO' => 'Somali',
                    'ZA' => 'South Africa',
                    'ES' => 'Spain',
                    'LK' => 'Sri Lanka',
                    'SD' => 'Sudan',
                    'SR' => 'Suriname',
                    'SZ' => 'Swaziland',
                    'SE' => 'Sweden',
                    'CH' => 'Switzerland',
                    'SY' => 'Syria',
                    'TW' => 'Taiwan',
                    'TJ' => 'Tajikstan',
                    'TZ' => 'Tanzania',
                    'TH' => 'Thailand',
                    'TG' => 'Togo',
                    'TO' => 'Tonga',
                    'TT' => 'Trinidad and Tobago',
                    'TN' => 'Tunisia',
                    'TR' => 'Turkey',
                    'TM' => 'Turkmenistan',
                    'TW' => 'Taiwan',
                    'UG' => 'Uganda',
                    'UA' => 'Ukraine',
                    'AE' => 'United Arab Emirates',
                    'GB' => 'United Kingdom',
                    'US' => 'United States',
                    'UY' => 'Uruguay',
                    'UZ' => 'Uzbekistan',
                    'VE' => 'Venezuela',
                    'VN' => 'Vietnam',
                    'YE' => 'Yemen',
                    'YU' => 'Yugoslavia',
                    'ZW' => 'Zimbabwe',
                    'ZR' => 'Zaire',
                    'ZM' => 'Zambia',
                );


                $orderidHandle = fopen("/tmp/paidorder/orderid.csv","r");
                $xxHandle = fopen("/tmp/paidorder/xx.txt","w");
                $empHandle = fopen("/tmp/paidorder/empty.txt","w");
                $proHandle = fopen("/tmp/paidorder/pro.txt","w");
                while(!feof($orderidHandle)) {
                    $orderid[] = fgetcsv($orderidHandle);
                }
                fclose($orderidHandle);

                $idHandle = fopen("/tmp/paidorder/order_id_txt.txt","w");
                fwrite($idHandle,var_export($orderid,true));
                fclose($idHandle);

                if(isset($orderid) && !empty($orderid[0])) {
                    $orderDetailHandle = fopen("/tmp/paidorder/orderdetail.csv","w");
                    $orderDetailLogHandle = fopen("/tmp/paidorder/order_detail_log.txt","w");
                    foreach($orderid as $oid) {
                        if(!empty($oid)) {
                            //for order
                            $result = $client->salesOrderInfo($session, $oid[0]);
                            $result = (array)$result;
                            $order_detail['order_id'] = $result['increment_id'];
                            $order_detail['create_time'] = strtotime($result['created_at']);
                            $order_detail['email'] = $result['customer_email'];
                            $order_detail['shipping_fee'] = $result['base_shipping_amount'];
                            $order_detail['total_money'] = $result['base_grand_total'];
                            $order_detail['ip'] = isset($result['remote_ip']) ? sprintf("%u", ip2long($result['remote_ip'])) : "1883904612";  //ip需要转换为数字
                            $order_detail['status'] = $result['status'];
                            $order_detail['update_time'] = strtotime($result['updated_at']);
                            $paymethod = (array)$result['payment'];
                            $order_detail['pay_type'] = $paymethod['method'];
                            $userinfo = (array)$result['shipping_address'];
                            $firstname = $userinfo['firstname']." first name";
                            $lastname = $userinfo['lastname']." last name";
                            if(isset($userinfo['middlename'])) {
                                $middlename = $userinfo['middlename']." middle name";
                            } else {
                                $middlename = "";
                            }
                            $order_detail['first_name'] = $firstname;
                            $order_detail['last_name'] = $lastname;
                            $order_detail['phone'] = $userinfo['telephone'];
                            $order_detail['zip_code'] = $userinfo['postcode'];
                            $order_detail['abbreviation'] = $userinfo['country_id'];
                            $order_detail['country'] = $countrys[$userinfo['country_id']];  //国家全称
                            $state = $userinfo['region']." user region";
                            $city = $userinfo['city'] . " user city";
                            $order_detail['state'] = $state;
                            $order_detail['city'] = $city;
                            $order_detail['address1'] = $userinfo['street'];

                            foreach($result['status_history'] as $paytypes) {
                                $paytypes = (array)$paytypes;
                                if($paytypes['status'] == 'processing') {
                                    fwrite($paytimeHandle,$paytypes['created_at']);
                                    $order_detail['pay_time'] = strtotime($paytypes['created_at']); //支付时间
                                    break;
                                }

                                /*                        foreach($paytypes as $paytype) {
                                                            if($paytype['status'] == 'processing') {
                                                                fwrite($paytimeHandle,$paytype['created_at']);
                                                                $order_detail['pay_time'] = strtotime($paytype['created_at']); //支付时间
                                                                break;
                                                            }
                                                        }*/
                            }

                            $products = array();
                            $order_product = array();
                            //for orderdetail
                            foreach($result['items'] as $key => $product) {
                                $product = (array)$product;
                                //将产品剥离出来，具体一个sku下面对应一个产品
                                if($product['product_type'] == "configurable") {
                                    $items = array('configurable' => '','simple' => '');
                                    $items['configurable'] = $product;
                                    $products[$product['sku']] = $items;
                                    $items = '';
                                    unset($items);
                                } else {
                                    $price = $product['price'];
                                    $price = (float)$price;
                                    $orginal_price = $product['original_price'];
                                    $orginal_price = (float)$orginal_price;
                                    if(isset($products[$product['sku']]) && empty($price) && empty($orginal_price)) {  // configurable
                                        $products[$product['sku']]['simple'] = $product;
                                        $products[$product['sku']]['configurable']['name'] = $products[$product['sku']]['simple']['name'];
                                        $products[$product['sku']]['configurable']['product_id'] = $products[$product['sku']]['simple']['product_id'];
                                        fwrite($empHandle,"config");
                                        fwrite($empHandle,"\n");
                                        //fwrite($empHandle,var_export($products[$product['sku']]['configurable'],true));
                                        //fwrite($empHandle,"\n");
                                        $tmp = $products[$product['sku']]['configurable'];
                                        $products[$product['sku']]['configurable'] = '';
                                        $products[$product['sku']]['simple'] = '';
                                        unset($products[$product['sku']]['configurable']);
                                        unset($products[$product['sku']]['simple']);
                                        //fwrite($empHandle,"tmp");
                                        //fwrite($empHandle,"\n");
                                        //fwrite($empHandle,var_export($tmp,true));
                                        //$products[$product['sku']] = $tmp;
                                        $order_product[] = $tmp;
                                        $tmp = '';
                                        unset($tmp);
                                    } else {
                                        //$products[$product['sku']] = $product;
                                        //fwrite($empHandle,"\n");
                                        fwrite($empHandle,"single");
                                        fwrite($empHandle,var_export($product,true));
                                        $order_product[] = $product;
                                    }
                                }

                                foreach($order_product as $pk => $pdt) {
                                    $product_item[$pk]['product_id'] = $pdt['product_id'];
                                    $product_item[$pk]['order_id'] = $pdt['order_id'];
                                    $product_item[$pk]['sku_id'] = 0;
                                    $product_item[$pk]['uniqid'] = $pdt['sku'];
                                    $qty = ceil($pdt['qty_ordered']) - ceil($pdt['qty_canceled']);
                                    $product_item[$pk]['total_money'] = $pdt['base_original_price']*$qty;
                                    $product_item[$pk]['pay_money'] = $pdt['base_original_price']*$qty;
                                    $product_item[$pk]['original_price'] = $pdt['original_price'];
                                    $product_item[$pk]['price'] = $pdt['base_original_price'];
                                    $product_item[$pk]['quantity'] = $qty;
                                    $product_item[$pk]['product_name'] = $pdt['name'];
                                    $product_item[$pk]['product_sku'] = $pdt['sku'];
                                    //$product_item[$pk]['product_type'] = $pdt['product_type'];
                                }
                            }
                            $order_detail['items'] = serialize($product_item);
                            $product_item = "";
                            unset($product_item);
                            $order_detail['middle_name'] = $middlename;
                            fputcsv($orderDetailHandle,$order_detail);
                            fwrite($xxHandle,var_export($products,true));
                            fwrite($proHandle,var_export($result,true));
                            $products = '';
                            $order_product = '';
                            unset($order_product);
                            unset($products);
                        }/* else {
                $order_detail['items'] = "empty";
                $order_detail['oid'] = $oid[0];

                fputcsv($orderDetailHandle,$order_detail);
            }*/

                        //write log
                        fwrite($orderDetailLogHandle,$oid);
                        fwrite($orderDetailLogHandle,"\n");
                        fwrite($orderDetailLogHandle,var_export($order_detail,true));
                    }
                    fclose($orderDetailHandle);
                    fclose($orderDetailLogHandle);
                    fclose($proHandle);
                    fclose($xxHandle);
                    fclose($paytimeHandle);
                    fclose($empHandle);
                    unlink("/tmp/paidorder/orderid.csv");
                    echo 1;
                }
            }
        }
    }

    /*
     * 方法说明：调用irulu的api进行用户数据的注册
     */
    public function uregisterAction() {

        $header = array(
            'X-App-Device: 10',
            'X-App-Version: 1.2.2',
            'X-Api-Version: v2.0',
            'NO-AUTH: 1',
            'X-UTM: ',
            'X-Uid: 0',
            'X-Did: sdfsfese',
            'X-Token: 55af66b41291512',
            //'X-FORWARDED-FOR: '. $ip,
            //'REMOTE-ADDR: '. $ip,
            //'CLIENT-IP: '. $ip
        );

        $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

        $url_user = "http://47.88.4.2:8082/register2/index?token=55af66b41291512";
        //$url_user ="http://api.account.irulu.com/register2/index?token=55af66b41291512";
        //$url_user ="http://112.74.26.100:10083/register2/index?token=55af66b41291512";
        //$url_order = "http://112.74.26.100:10083/order2/createOrder?token=55af66b41291512";

        $handle = fopen("/tmp/users/userinfo.csv","r");
        while(!feof($handle)) {
            $users_info[] = fgetcsv($handle);
        }

        $useriHandle = fopen("/tmp/users/user_info_txt.txt","w");
        fwrite($useriHandle,var_export($users_info,true));
        fclose($useriHandle);

        if(isset($users_info) && !empty($users_info[0])) {
            $registerUserHandle = fopen("/tmp/users/register_user.csv","a+");
            foreach($users_info as $uk => $user_info) {
                if(!empty($user_info)) {
                    $str_pos = strpos($user_info[3],":");
                    $pwd = substr($user_info[3],0,$str_pos);
                    $salt = substr($user_info[3],$str_pos+1);

                    $user_data =  array(
                        'email'             => $user_info[0],
                        'lastname'            => $user_info[1],
                        'firstname'                => $user_info[2],
                        'password'             => $pwd,//$user_info[3],
                        'salt'             => $salt,//$user_info[3],
                        'register_date'                 => $user_info[4],
                        'last_login_date'              => 1,
                        'user_type'           => '3',
                        'sex'         => 'F',  // 男：M   女：F
                        'post_addr'                => $user_info[5], //邮寄地址
                        'channel'              => 'pcweb',
                        'login_type'              => '1', //用户登录类型
                        'phone'              => $user_info[6],
                    );

                    //调用irulu的api
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url_user);  //1、url
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                    curl_setopt($ch, CURLOPT_POST, 1);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $user_data); //2、data
                    $res = curl_exec($ch);

                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    $register_info[$uk]['uid'] = $res;
                    $register_info[$uk]['email'] = $user_info[0];
                    fputcsv($registerUserHandle,$register_info[$uk]);
                }
            }
            fclose($registerUserHandle);
            fclose($handle);
            unlink("/tmp/users/userinfo.csv");
            echo 1;
        }
    }


    /*
     * 方法说明：获取magento订单并通过irulu的api保存到数据库  这个方法也要在下午3点以后执行，因为其需要orderdetail.csv文件
     */
    public function oaddAction() {
        date_default_timezone_set("Asia/Shanghai");
        $weekday = date("w",time());

        switch($weekday) {
            case 0: $action = 0;break;
            case 6: $action = 1;break;
            default:$action = 1;break;
        }

        if($action) {
            if(file_exists("/tmp/paidorder/orderdetail.csv")) {
                $header = array(
                    'X-App-Device: 10',
                    'X-App-Version: 1.2.2',
                    'X-Api-Version: v2.0',
                    'NO-AUTH: 1',
                    'X-UTM: ',
                    'X-Uid: 0',
                    'X-Did: sdfsfese',
                    'X-Token: 55af66b41291512',
                    //'X-FORWARDED-FOR: '. $ip,
                    //'REMOTE-ADDR: '. $ip,
                    //'CLIENT-IP: '. $ip
                );

                $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

                //$url_user = "http://api.account.irulu.com/register2/index?token=55af66b41291512";
                $url_order = "http://47.88.4.2/order2/createOrder?token=55af66b41291512";
                $url_order_detail = "http://47.88.4.2/order2/insertOrderDetail?token=55af66b41291512";
                $url_getuid ="http://47.88.4.2:8082/register2/getuid?token=55af66b41291512";

                /*        $url_order = "http://112.74.26.100:10084/order2/createOrder?token=55af66b41291512";
                        $url_order_detail = "http://112.74.26.100:10084/order2/insertOrderDetail?token=55af66b41291512";
                        $url_getuid ="http://112.74.26.100:10083/register2/getuid?token=55af66b41291512";*/

                $fail_get_uid_handle = fopen("/tmp/paidorder/fail_get_uid.log","a+");
                $fail_get_uid_csv_handle = fopen("/tmp/paidorder/fail_get_uid.csv","a+");
				$fail_get_uid_csv_bak_handle = fopen("/tmp/paidorder/fail_get_uid_bak.csv","a+");
                $productHandle = fopen("/tmp/paidorder/product.txt","a+");
                $orderDetailHandle = fopen("/tmp/paidorder/orderdetail.csv","r");
                $orderLogHandle = fopen("/tmp/paidorder/order_log.txt","a+");
                $showHandle = fopen("/tmp/paidorder/show_log.txt","w");
                $uidHandle = fopen("/tmp/paidorder/uid.csv","w");
                $makeupHandle = fopen("/tmp/paidorder/makeup.csv","a");
                while(!feof($orderDetailHandle)) {
                    $orders_info[] = fgetcsv($orderDetailHandle);
                }

                if(isset($orders_info) && !empty($orders_info[0])) {
                    foreach($orders_info as $ok => $order_info) {
                        if(!empty($order_info)) {
                            //根据email获取用户的uid
                            $email = array('email' => $order_info[2]);
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url_getuid);  //1、url
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                            curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                            curl_setopt($ch, CURLOPT_POST, 1);

                            curl_setopt($ch, CURLOPT_POSTFIELDS, $email); //2、data
                            $uid = curl_exec($ch);
                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            curl_close($ch);
                            if(empty($uid) || ($uid=='') || ($uid==' ')) {
                                $fail_uid_arr = array(
                                    'email' => $order_info[2],
                                    'time' => date("Y-m-d H:i:s",time()),
                                    'order_id' => $order_info[0]
                                );
                                fputcsv($fail_get_uid_csv_handle,$fail_uid_arr);
                                fputcsv(fail_get_uid_csv_bak_handle,$fail_uid_arr);
                                fwrite($fail_get_uid_handle,var_export($fail_uid_arr,true));

                                //将没有uid的订单写入补录文件
                                fputcsv($makeupHandle,$order_info);

                            } else {
                                $u[0] = $uid;
                                fputcsv($uidHandle,$u);
                                if($order_info[8] == "paypal_express") {
                                    $pay_type = 1;
                                } else {
                                    $pay_type = 2;
                                }

                                $firstname = substr($order_info[9],0,strpos($order_info[9]," first name"));
                                $lastname = substr($order_info[10],0,strpos($order_info[10]," last name"));
                                if(!empty($order_info[20])) {
                                    $middlename = substr($order_info[20],0,strpos($order_info[20]," middle name"));
                                } else {
                                    $middlename = "";
                                }
                                $fullname = $firstname." ".$middlename." ".$lastname;
                                $state = substr($order_info[15],0,strpos($order_info[15]," user region"));
                                $city = substr($order_info[16],0,strpos($order_info[16]," user city"));

                                $data_order = array(
                                    'order_id' => $order_info[0],
                                    'uid' => $uid,
                                    'mid' => 1,
                                    'total_money' => $order_info[4],  //这里需要在购物的时候加个购物券测试是否是折扣后的价格
                                    'pay_money' => $order_info[4],
                                    'receive_money' => $order_info[4],
                                    'shipping_fee' => $order_info[3],
                                    'savings_fee' => 0,
                                    'lable' => 'paid',
                                    'insurance_fee' => 0, //商品物流保险费用
                                    'first_name' => $firstname,
                                    'last_name' => $lastname,
                                    'full_name' => $fullname,
                                    'phone' => $order_info[11],
                                    'email' => $order_info[2],
                                    'zip_code' => $order_info[12],
                                    'abbreviation' => $order_info[13], //国家缩写
                                    'country' => $order_info[14],
                                    'state' => $state,
                                    'city' => $city,
                                    'address1' => $order_info[17],
                                    'address2' => '',
                                    'status' => 30, //1 未付款；3 已付款; 30 已付款以确认
                                    'device_id' => 'pcweb',
                                    'platform' => 0,
                                    'order_source' => 10,
                                    'channel' => "pcweb",
                                    'ip' => !empty($order_info[5]) ? $order_info[5] : "1883904612",  //ip需要转换为数字,
                                    'pay_time' => $order_info[18],
                                    'create_time' => $order_info[1],
                                    'update_time' => $order_info[7],
                                    'expired_time' => 0,
                                    'pay_type' => $pay_type//$order_info[8]  //1：Paypal、2：信用卡'  支付方式这里需要改为数字
                                );
                                fwrite($showHandle,var_export($data_order,true));
                                //调用irulu的api
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url_order);  //1、url
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                                curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                                curl_setopt($ch, CURLOPT_POST, 1);

                                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_order); //2、data
                                $res = curl_exec($ch);

                                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                                curl_close($ch);
                                $order_log[$ok]['orderid'] = $order_info[0];
                                $order_log[$ok]['oid'] = $res;
                                $order_log[$ok]['email'] = $order_info[2];
                                $order_log[$ok]['mess'] = $uid;
                                //fputcsv($orderLogHandle, $order_log[$ok]);
                                fwrite($orderLogHandle,var_export($order_log[$ok],true));
                                fwrite($orderLogHandle,"\n");
                                $order_products = unserialize($order_info[19]);
                                foreach ($order_products as $key => $product) {
                                    if (!empty($product)) {
										$isExists = 0;
                                        if(isset($order_detail_data)) {
                                            foreach($order_detail_data as $k => $v) {
                                                if($v['uniqid'] == $product['product_sku']) {
                                                    $isExists = 1;
                                                    break;
                                                }
                                            }
                                        }
										if(!$isExists) {
												$order_detail_data = array(    //一个订单中有多个产品的时候，需要多个这样的数据结构
												'order_id' => $data_order['order_id'],
												'uid' => $uid,  //这里需要填写uid
												'parent_id' => 0,
												'product_id' => $product['product_id'],
												'sku_id' => 0,
												// 'total_money'           => $product['originalPrice'] * $product['quantity'],
												'total_money' => $product['total_money'],
												'pay_money' => $product['pay_money'],
												'original_price' => $product['original_price'],
												'price' => $product['price'],
												'quantity' => $product['quantity'],
												'product_name' => $product['product_name'],
												'product_sku' => '',
												'product_image' => '',
												'uniqid' => $product['product_sku'],  //sku
												'promotion_product_id' => 0
											);

											$ch = curl_init();
											curl_setopt($ch, CURLOPT_URL, $url_order_detail);  //1、url
											curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
											curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

											curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
											curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

											curl_setopt($ch, CURLOPT_POST, 1);

											curl_setopt($ch, CURLOPT_POSTFIELDS, $order_detail_data); //2、data
											$res = curl_exec($ch);

											$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
											curl_close($ch);
											$order_log[$ok]['did'] = $res;
											$order_log[$ok]['email'] = $order_info[2];
											$order_log[$ok]['product'] = $order_detail_data;
											$order_log[$ok]['uid'] = $uid;
											//fputcsv($productHandle, $order_log[$ok]);
											fwrite($productHandle,var_export($order_log[$ok],true));
											fwrite($productHandle,"\n");
                                    
										}
                                    }
                                }

                            }
                        }/* else {
                    $order_log[$ok]['did'] = $res;
                    $order_log[$ok]['order'] = $data_order['order_id'];
                    $order_log[$ok]['res'] = "empty";
                    fwrite($productHandle,var_export($order_log[$ok]));
                    fwrite($productHandle,"\n");
                }*/
                    }
                    fclose($productHandle);
                    fclose($orderDetailHandle);
                    fclose($orderLogHandle);
                    fclose($showHandle);
                    fclose($uidHandle);
                    fclose($makeupHandle);
                    fclose($fail_get_uid_handle);
                    fclose($fail_get_uid_csv_handle);
                    fclose(fail_get_uid_csv_bak_handle);
                    unlink("/tmp/paidorder/orderdetail.csv");
                    echo 1;
                }
            }
        }
    }

    /*
     * 订单补录功能
     */
    public function makeuporderAction() {
        if(file_exists("/tmp/paidorder/makeup.csv")) {

            //获取

            $fail_get_uid_handle = fopen("/tmp/paidorder/makeup_fail_get_uid.log","a+");
            $fail_get_uid_csv_handle = fopen("/tmp/paidorder/makeup_fail_get_uid.csv","a+");
            $productHandle = fopen("/tmp/paidorder/makeup_product.txt","a+");
            $orderLogHandle = fopen("/tmp/paidorder/makeup_order_log.txt","a+");
            $showHandle = fopen("/tmp/paidorder/makeup_show_log.txt","w");
            $uidHandle = fopen("/tmp/paidorder/makeup_uid.csv","w");

            $header = array(
                'X-App-Device: 10',
                'X-App-Version: 1.2.2',
                'X-Api-Version: v2.0',
                'NO-AUTH: 1',
                'X-UTM: ',
                'X-Uid: 0',
                'X-Did: sdfsfese',
                'X-Token: 55af66b41291512'
            );

            $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

            $url_order = "http://47.88.4.2/order2/createOrder?token=55af66b41291512";
            $url_order_detail = "http://47.88.4.2/order2/insertOrderDetail?token=55af66b41291512";
            $url_getuid ="http://47.88.4.2:8082/register2/getuid?token=55af66b41291512";

            $makeupHandle = fopen("/tmp/paidorder/makeup.csv","r");
            while(!feof($makeupHandle)) {
                $makeup_order[] = fgetcsv($makeupHandle);
            }
            fclose($makeupHandle);
            foreach($makeup_order as $ok => $order_info) {
                if(!empty($order_info)) {
                    //根据email获取用户的uid
                    $email = array('email' => $order_info[2]);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url_getuid);  //1、url
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                    curl_setopt($ch, CURLOPT_POST, 1);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $email); //2、data
                    $uid = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    if(empty($uid)) {
                        $fail_uid_arr = array(
                            'email' => $order_info[2],
                            'uid' => $uid,
                            'order_id' => $order_info[0]
                        );
                        fputcsv($fail_get_uid_csv_handle,$fail_uid_arr);
                        fwrite($fail_get_uid_handle,var_export($fail_uid_arr,true));
                    } else {
                        $u[0] = $uid;
                        fputcsv($uidHandle,$u);
                        if($order_info[8] == "paypal_express") {
                            $pay_type = 1;
                        } else {
                            $pay_type = 2;
                        }

                        $firstname = substr($order_info[9],0,strpos($order_info[9]," first name"));
                        $lastname = substr($order_info[10],0,strpos($order_info[10]," last name"));
                        if(!empty($order_info[20])) {
                            $middlename = substr($order_info[20],0,strpos($order_info[20]," middle name"));
                        } else {
                            $middlename = "";
                        }
                        $fullname = $firstname." ".$middlename." ".$lastname;
                        $state = substr($order_info[15],0,strpos($order_info[15]," user region"));
                        $city = substr($order_info[16],0,strpos($order_info[16]," user city"));

                        $data_order = array(
                            'order_id' => $order_info[0],
                            'uid' => $uid,
                            'mid' => 1,
                            'total_money' => $order_info[4],  //这里需要在购物的时候加个购物券测试是否是折扣后的价格
                            'pay_money' => $order_info[4],
                            'receive_money' => $order_info[4],
                            'shipping_fee' => $order_info[3],
                            'savings_fee' => 0,
                            'lable' => 'paid',
                            'insurance_fee' => 0, //商品物流保险费用
                            'first_name' => $firstname,
                            'last_name' => $lastname,
                            'full_name' => $fullname,
                            'phone' => $order_info[11],
                            'email' => $order_info[2],
                            'zip_code' => $order_info[12],
                            'abbreviation' => $order_info[13], //国家缩写
                            'country' => $order_info[14],
                            'state' => $state,
                            'city' => $city,
                            'address1' => $order_info[17],
                            'address2' => '',
                            'status' => 30, //1 未付款；3 已付款; 30 已付款以确认
                            'device_id' => 'pcweb',
                            'platform' => 0,
                            'order_source' => 10,
                            'channel' => "pcweb",
                            'ip' => !empty($order_info[5]) ? $order_info[5] : "1883904612",  //ip需要转换为数字,
                            'pay_time' => $order_info[18],
                            'create_time' => $order_info[1],
                            'update_time' => $order_info[7],
                            'expired_time' => 0,
                            'pay_type' => $pay_type//$order_info[8]  //1：Paypal、2：信用卡'  支付方式这里需要改为数字
                        );
                        fwrite($showHandle,var_export($data_order,true));
                        //调用irulu的api
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url_order);  //1、url
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                        curl_setopt($ch, CURLOPT_POST, 1);

                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_order); //2、data
                        $res = curl_exec($ch);

                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);
                        $order_log[$ok]['orderid'] = $order_info[0];
                        $order_log[$ok]['oid'] = $res;
                        $order_log[$ok]['email'] = $order_info[2];
                        $order_log[$ok]['mess'] = $uid;
                        //fputcsv($orderLogHandle, $order_log[$ok]);
                        fwrite($orderLogHandle,var_export($order_log[$ok],true));
                        fwrite($orderLogHandle,"\n");
                        $order_products = unserialize($order_info[19]);
                        foreach ($order_products as $key => $product) {
                            if (!empty($product)) {
                                $order_detail_data = array(    //一个订单中有多个产品的时候，需要多个这样的数据结构
                                    'order_id' => $data_order['order_id'],
                                    'uid' => $uid,  //这里需要填写uid
                                    'parent_id' => 0,
                                    'product_id' => $product['product_id'],
                                    'sku_id' => 0,
                                    // 'total_money'           => $product['originalPrice'] * $product['quantity'],
                                    'total_money' => $product['total_money'],
                                    'pay_money' => $product['pay_money'],
                                    'original_price' => $product['original_price'],
                                    'price' => $product['price'],
                                    'quantity' => $product['quantity'],
                                    'product_name' => $product['product_name'],
                                    'product_sku' => '',
                                    'product_image' => '',
                                    'uniqid' => $product['product_sku'],  //sku
                                    'promotion_product_id' => 0
                                );

                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url_order_detail);  //1、url
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                                curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                                curl_setopt($ch, CURLOPT_POST, 1);

                                curl_setopt($ch, CURLOPT_POSTFIELDS, $order_detail_data); //2、data
                                $res = curl_exec($ch);

                                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                                curl_close($ch);
                                $order_log[$ok]['did'] = $res;
                                $order_log[$ok]['email'] = $order_info[2];
                                $order_log[$ok]['product'] = $order_detail_data;
                                $order_log[$ok]['uid'] = $uid;
                                //fputcsv($productHandle, $order_log[$ok]);
                                fwrite($productHandle,var_export($order_log[$ok],true));
                                fwrite($productHandle,"\n");
                            }
                        }
                        unset($makeup_order[$ok]);
                    }
                }
            }

            unlink("/tmp/paidorder/makeup.csv");
            if(!empty($makeup_order)) {
                $makeup2Handle = fopen("/tmp/paidorder/makeup.csv","a");
                foreach($makeup_order as $kmo => $vmo) {
                    fputcsv($makeup2Handle,$vmo);
                }
                fclose($makeup2Handle);
            }

            fclose($fail_get_uid_handle);
            fclose($fail_get_uid_csv_handle);
            fclose($productHandle);
            fclose($orderLogHandle);
            fclose($showHandle);
            fclose($uidHandle);
        }
    }


    /*
     * 活动统计
     * 方法说明 统计：
     * 总销售额
        总成功订单量（包括processing+paid）
        X1S,BL20,X1这三个优惠码成功使用了多少次
        英和俄两站分别带来多少订单量(  'store_name' => 'Main Website
        Main Website Store
        English',)

             'store_name' => 'Main Website
        Main Website Store
        Russian',

        多少订单是属于pending状态
     */
    public function statisticsorderlistAction() {
        if(file_exists("/tmp/activity/statistics_orderid.csv")) {
            unlink("/tmp/activity/statistics_orderid.csv");
        }
        $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $session = $client->login('irulu', 'iruluapitest');
        date_default_timezone_set("America/Los_Angeles");

        $total_sales = 0;  //销售总额
        $order_success_count = 0; //成功订单量
        $statistics_info = array(
            "total_sales" => 0,
            "order_success_count" => 0,
            "X1S" => 0,
            "BL20" => 0,
            "X1" => 0,
            "pendging" => 0,
            "english_store" => 0,
            "russian_store" => 0
        );
        $activity = 1;

        if($activity) {  //正式活动时间
            $activity_start_time = 1498636800;  //活动开始时间
            $activity_end_time = 1498895999; //活动结束时间
        } else {  //测试用时间
            $activity_start_time = 1498636800;  //活动开始时间
            //测试结束时间20170701 12点
            $activity_end_time = 1498885199; //活动结束时间
        }

        $acitvity_start_time_date = date("Y-m-d H:i:s",$activity_start_time);

        $activeHandle = fopen("/tmp/activity/active_order.csv","w");
        $filter = array('complex_filter' => array(array('key' => 'created_at', 'value' => array('key' => 'gt','value'=>$acitvity_start_time_date))));
        $order_list = $client->salesOrderList($session,$filter);
        if(!empty($order_list)) {
            $orderidHandle = fopen("/tmp/activity/statistics_orderid.csv","w");
            $orderSumHandle = fopen("/tmp/activity/statistics_order_sum.csv","a+");
            $pendingOrderHandle = fopen("/tmp/activity/statistics_pending_orderid.csv","w");
            $orderHandle = fopen("/tmp/activity/statistics_orders.txt","w");

            //筛选订单时间在活动范围内的订单
            foreach($order_list as $k => $od) {
                $od = (array)$od;
                $order_time = strtotime($od['created_at']);
                if(($order_time <= $activity_end_time)/* && ($order_time > $activity_start_time)*/) {
                    continue;
                } else {
                    unset($order_list[$k]);
                }
            }

            foreach($order_list as $key => $order) {
                $order = (array)$order;

                if(($order['status'] == 'paid') || $order['status'] == 'processing') {
                    //统计总销售额、成功订单总量、coupon使用量
                    $total_sales += $order['base_grand_total'];
                    $order_success_count++;

                    //统计coupon使用量
                    if(isset($order['coupon_code'])) {
                        switch($order['coupon_code']) {
                            case "X1S": $statistics_info["X1S"]++;break;
                            case "BL20": $statistics_info["BL20"]++;break;
                            case "X1": $statistics_info["X1"]++;break;
                        }
                    }

                    $s_name_match_english = preg_match("/English/",$order['store_name']);
                    $s_name_match_russian = preg_match("/Russian/",$order['store_name']);
                    if($s_name_match_english) {
                        $statistics_info["english_store"]++;
                    } else if($s_name_match_russian) {
                        $statistics_info["russian_store"]++;
                    }
                    continue;
                } else {
                    if($order['status'] == 'pending') {
                        $statistics_info["pendging"]++;
                        $statistics_pending_orderid[0]=$order['increment_id'];
                        fputcsv($pendingOrderHandle,$statistics_pending_orderid);
                    }
                    unset($order_list[$key]);
                }
            }
            $statistics_info["total_sales"] = $total_sales;
            $statistics_info["order_success_count"] = $order_success_count;
            fwrite($orderHandle,var_export($order_list,true));


            foreach($order_list as $order) {
                $order = (array)$order;
                $orderid[0]=$order['increment_id'];

                fputcsv($orderidHandle,$orderid);
                fputcsv($orderSumHandle,$orderid);
            }

            fputcsv($activeHandle,$statistics_info);
            fclose($orderidHandle);
            fclose($orderSumHandle);
            fclose($orderHandle);
            fclose($activeHandle);
            fclose($pendingOrderHandle);
            echo 1;
        }
    }

    /*
     * 方法说明：统计sku 每个SKU卖的分别数量和销售额和单价
     */
    public function statisticsorderskuAction() {
        date_default_timezone_set("Asia/Shanghai");
        $weekday = date("w",time());

        switch($weekday) {
            case 0: $action = 1;break;
            case 6: $action = 1;break;
            default:$action = 1;break;
        }

        if($action) {
            if(file_exists("/tmp/activity/statistics_orderdetail.csv")) {
                unlink("/tmp/activity/statistics_orderdetail.csv");
            }

            if(file_exists("/tmp/activity/statistics_orderid.csv")) {
                $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
                $session = $client->login('irulu', 'iruluapitest');
                $countrys_sx = array (
                    'AO' => 'Angola',
                    'AF' => 'Afghanistan',
                    'AL' => 'Albania',
                    'DZ' => 'Algeria',
                    'AD' => 'Andorra',
                    'AI' => 'Anguilla',
                    'AG' => 'Antigua and Barbuda',
                    'AR' => 'Argentina',
                    'AM' => 'Armenia',
                    'AU' => 'Australia',
                    'AT' => 'Austria',
                    'AZ' => 'Azerbaijan',
                    'BS' => 'Bahamas',
                    'BH' => 'Bahrain',
                    'BD' => 'Bangladesh',
                    'BB' => 'Barbados',
                    'BY' => 'Belarus',
                    'BE' => 'Belgium',
                    'BZ' => 'Belize',
                    'BJ' => 'Benin',
                    'BM' => 'Bermuda Is.',
                    'BO' => 'Bolivia',
                    'BW' => 'Botswana',
                    'BR' => 'Brazil',
                    'BN' => 'Brunei',
                    'BG' => 'Bulgaria',
                    'BF' => 'Burkina-faso',
                    'MM' => 'Burma',
                    'BI' => 'Burundi',
                    'CM' => 'Cameroon',
                    'CA' => 'Canada',
                    'CF' => 'Central African Republic',
                    'TD' => 'Chad',
                    'CL' => 'Chile',
                    'CN' => 'China',
                    'CO' => 'Colombia',
                    'CG' => 'Congo',
                    'CK' => 'Cook Is.',
                    'CR' => 'Costa Rica',
                    'CU' => 'Cuba',
                    'CY' => 'Cyprus',
                    'CZ' => 'Czech Republic',
                    'DK' => 'Denmark',
                    'DJ' => 'Djibouti',
                    'DO' => 'Dominica Rep.',
                    'EC' => 'Ecuador',
                    'EG' => 'Egypt',
                    'SV' => 'EI Salvador',
                    'EE' => 'Estonia',
                    'ET' => 'Ethiopia',
                    'FJ' => 'Fiji',
                    'FI' => 'Finland',
                    'FR' => 'France',
                    'GF' => 'French Guiana',
                    'GA' => 'Gabon',
                    'GM' => 'Gambia',
                    'GE' => 'Georgia',
                    'DE' => 'Germany',
                    'GH' => 'Ghana',
                    'GI' => 'Gibraltar',
                    'GR' => 'Greece',
                    'GD' => 'Grenada',
                    'GU' => 'Guam',
                    'GT' => 'Guatemala',
                    'GN' => 'Guinea',
                    'GY' => 'Guyana',
                    'HT' => 'Haiti',
                    'HN' => 'Honduras',
                    'HK' => 'Hong Kong',
                    'HU' => 'Hungary',
                    'IS' => 'Iceland',
                    'IN' => 'India',
                    'ID' => 'Indonesia',
                    'IR' => 'Iran',
                    'IQ' => 'Iraq',
                    'IE' => 'Ireland',
                    'IL' => 'Israel',
                    'IT' => 'Italy',
                    'JM' => 'Jamaica',
                    'JP' => 'Japan',
                    'JO' => 'Jordan',
                    'KH' => 'Kampuchea (Cambodia )',
                    'KZ' => 'Kazakstan',
                    'KE' => 'Kenya',
                    'KR' => 'Korea',
                    'KW' => 'Kuwait',
                    'KG' => 'Kyrgyzstan',
                    'LA' => 'Laos',
                    'LV' => 'Latvia',
                    'LB' => 'Lebanon',
                    'LS' => 'Lesotho',
                    'LR' => 'Liberia',
                    'LY' => 'Libya',
                    'LI' => 'Liechtenstein',
                    'LT' => 'Lithuania',
                    'LU' => 'Luxembourg',
                    'MO' => 'Macao',
                    'MG' => 'Madagascar',
                    'MW' => 'Malawi',
                    'MY' => 'Malaysia',
                    'MV' => 'Maldives',
                    'ML' => 'Mali',
                    'MT' => 'Malta',
                    'MU' => 'Mauritius',
                    'MX' => 'Mexico',
                    'MD' => 'Moldova, Republic of',
                    'MC' => 'Monaco',
                    'MN' => 'Mongolia',
                    'MS' => 'Montserrat Is',
                    'MA' => 'Morocco',
                    'MZ' => 'Mozambique',
                    'NA' => 'Namibia',
                    'NR' => 'Nauru',
                    'NP' => 'Nepal',
                    'NL' => 'Netherlands',
                    'NZ' => 'New Zealand',
                    'NI' => 'Nicaragua',
                    'NE' => 'Niger',
                    'NG' => 'Nigeria',
                    'KP' => 'North Korea',
                    'NO' => 'Norway',
                    'OM' => 'Oman',
                    'PK' => 'Pakistan',
                    'PA' => 'Panama',
                    'PG' => 'Papua New Cuinea',
                    'PY' => 'Paraguay',
                    'PE' => 'Peru',
                    'PH' => 'Philippines',
                    'PL' => 'Poland',
                    'PF' => 'French Polynesia',
                    'PT' => 'Portugal',
                    'PR' => 'Puerto Rico',
                    'QA' => 'Qatar',
                    'RO' => 'Romania',
                    'RU' => 'Russia',
                    'LC' => 'St.Lucia',
                    'VC' => 'St.Vincent',
                    'SM' => 'San Marino',
                    'ST' => 'Sao Tome and Principe',
                    'SA' => 'Saudi Arabia',
                    'SN' => 'Senegal',
                    'SC' => 'Seychelles',
                    'SL' => 'Sierra Leone',
                    'SG' => 'Singapore',
                    'SK' => 'Slovakia',
                    'SI' => 'Slovenia',
                    'SB' => 'Solomon Is',
                    'SO' => 'Somali',
                    'ZA' => 'South Africa',
                    'ES' => 'Spain',
                    'LK' => 'Sri Lanka',
                    'SD' => 'Sudan',
                    'SR' => 'Suriname',
                    'SZ' => 'Swaziland',
                    'SE' => 'Sweden',
                    'CH' => 'Switzerland',
                    'SY' => 'Syria',
                    'TW' => 'Taiwan',
                    'TJ' => 'Tajikstan',
                    'TZ' => 'Tanzania',
                    'TH' => 'Thailand',
                    'TG' => 'Togo',
                    'TO' => 'Tonga',
                    'TT' => 'Trinidad and Tobago',
                    'TN' => 'Tunisia',
                    'TR' => 'Turkey',
                    'TM' => 'Turkmenistan',
                    'TW' => 'Taiwan',
                    'UG' => 'Uganda',
                    'UA' => 'Ukraine',
                    'AE' => 'United Arab Emirates',
                    'GB' => 'United Kingdom',
                    'US' => 'United States',
                    'UY' => 'Uruguay',
                    'UZ' => 'Uzbekistan',
                    'VE' => 'Venezuela',
                    'VN' => 'Vietnam',
                    'YE' => 'Yemen',
                    'YU' => 'Yugoslavia',
                    'ZW' => 'Zimbabwe',
                    'ZR' => 'Zaire',
                    'ZM' => 'Zambia',
                );

                $skus = array();
                $sku_item = array(
                    "sku" => '',
                    "count" => 0,
                    "single_price" => 0,
                    "total_sales" => 0
                );
                $country = array();
                $cry_item = array(
                    "country" => '',
                    "count" => 0,
                    "total_sales" => 0
                );
                $pay_method = array();
                $pay_item = array(
                    "method" => '',
                    "count" => 0,
                );
                $orderidHandle = fopen("/tmp/activity/statistics_orderid.csv","r");
                $countryHandle = fopen("/tmp/activity/statistics_country.csv","w");
                $proHandle = fopen("/tmp/activity/statistics_pro.log","w");
                $pm = fopen("/tmp/activity/pm.txt","w");
                $paymethodHandle = fopen("/tmp/activity/paymethod.csv","w");
                $skuHandle = fopen("/tmp/activity/skulist.csv","w");
                while(!feof($orderidHandle)) {
                    $orderid[] = fgetcsv($orderidHandle);
                }
                fclose($orderidHandle);

                $idHandle = fopen("/tmp/activity/statistics_orderid.txt","w");
                fwrite($idHandle,var_export($orderid,true));
                fclose($idHandle);
                $sum_count = 0;
                if(isset($orderid) && !empty($orderid[0])) {
                    foreach($orderid as $oid) {
                        if(!empty($oid)) {
                            //for order
                            $result = $client->salesOrderInfo($session, $oid[0]);
                            $result = (array)$result;
                            //fwrite($proHandle,var_export($result,true));
                            //统计支付方式使用量
                            $payment = (array)$result['payment'];
                            fwrite($pm,var_export($payment,true));
                            if(isset($pay_method[$payment['method']])) {
                                $pay_method[$payment['method']]['count'] += 1;
                            } else {
                                $pay_item['method'] = $payment['method'];
                                $pay_item['count'] += 1;
                                $pay_method[$payment['method']] = $pay_item;
                                $pay_item['method'] = '';
                                $pay_item['count'] = 0;
                            }
                            //统计每个国家购买量
                            $countrys = (array)$result['shipping_address'];
                            if(isset($country[$countrys['country_id']])) {
                                $country[$countrys['country_id']]['count']++;
                                $country[$countrys['country_id']]['total_sales'] += $result['base_grand_total'];
                                $sum_count++;
                            } else {
                                $cry_item['country'] = $countrys_sx[$countrys['country_id']];
                                $cry_item['count'] ++;
                                $cry_item['total_sales'] = $result['base_grand_total'];
                                $country[$countrys['country_id']] = $cry_item;
                                $cry_item['country'] = '';
                                $cry_item['count'] = 0;
                                $cry_item['total_sales'] = 0;
                                $sum_count++;
                            }

                            //统计sku
                            foreach($result['items'] as $key => $product) {
                                $product = (array)$product;
                                //将产品剥离出来，具体一个sku下面对应一个产品
                                if($product['product_type'] == "configurable") {
                                    if(isset($skus[$product['sku']])) {
                                        $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                        $skus[$product['sku']]['count'] +=1;
                                        $skus[$product['sku']]['total_sales'] += $product['base_original_price']*$qty;
                                    } else {
                                        $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                        $sku_item['sku'] = $product['sku'];
                                        $sku_item['count'] +=1;
                                        $sku_item['single_price'] = $product['base_original_price'];
                                        $sku_item['total_sales'] = $product['base_original_price']*$qty;

                                        $skus[$product['sku']] = $sku_item;
                                    }
                                } else {
                                    $price = $product['price'];
                                    $price = (float)$price;
                                    $orginal_price = $product['original_price'];
                                    $orginal_price = (float)$orginal_price;
                                    if(isset($skus[$product['sku']]) && empty($price) && empty($orginal_price)) {  // configurable

                                    } else {
                                        if(isset($skus[$product['sku']])) {
                                            $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                            $skus[$product['sku']]['count'] +=1;
                                            $skus[$product['sku']]['total_sales'] += $product['base_original_price']*$qty;
                                        } else {
                                            $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                            $sku_item['sku'] = $product['sku'];
                                            $sku_item['count'] +=1;
                                            $sku_item['single_price'] = $product['base_original_price'];
                                            $sku_item['total_sales'] = $product['base_original_price']*$qty;

                                            $skus[$product['sku']] = $sku_item;
                                        }
                                    }
                                }

                                $sku_item['sku'] = '';
                                $sku_item['count'] = 0;
                                $sku_item['single_price'] = 0;
                                $sku_item['total_sales'] = 0;
                            }
                            $products = '';
                            $order_product = '';
                            unset($order_product);
                            unset($products);
                        }
                    }
                    foreach($skus as $sku) {
                        fputcsv($skuHandle,$sku);
                    }

                    foreach($country as $c) {
                        fputcsv($countryHandle,$c);
                    }

                    foreach($pay_method as $p) {
                        fputcsv($paymethodHandle,$p);
                    }
                    fclose($skuHandle);
                    fclose($countryHandle);
                    fclose($paymethodHandle);
                    fclose($pm);
                    fwrite($proHandle,$sum_count);
                    fclose($proHandle);
                    unlink("/tmp/activity/statistics_orderid.csv");
                    echo 1;
                }
            }
        }
    }

    /*
     * 方法说明：统计活动中状态为pending的订单
     */
    public function statisticspendingorderAction() {
        date_default_timezone_set("Asia/Shanghai");
        $weekday = date("w",time());

        switch($weekday) {
            case 0: $action = 1;break;
            case 6: $action = 1;break;
            default:$action = 1;break;
        }

        if($action) {
            if(file_exists("/tmp/activity/statistics_pending_orderdetail.csv")) {
                unlink("/tmp/activity/statistics_pending_orderdetail.csv");
            }

            if(file_exists("/tmp/activity/statistics_pending_orderid.csv")) {
                $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
                $session = $client->login('irulu', 'iruluapitest');
                $countrys_sx = array (
                    'AO' => 'Angola',
                    'AF' => 'Afghanistan',
                    'AL' => 'Albania',
                    'DZ' => 'Algeria',
                    'AD' => 'Andorra',
                    'AI' => 'Anguilla',
                    'AG' => 'Antigua and Barbuda',
                    'AR' => 'Argentina',
                    'AM' => 'Armenia',
                    'AU' => 'Australia',
                    'AT' => 'Austria',
                    'AZ' => 'Azerbaijan',
                    'BS' => 'Bahamas',
                    'BH' => 'Bahrain',
                    'BD' => 'Bangladesh',
                    'BB' => 'Barbados',
                    'BY' => 'Belarus',
                    'BE' => 'Belgium',
                    'BZ' => 'Belize',
                    'BJ' => 'Benin',
                    'BM' => 'Bermuda Is.',
                    'BO' => 'Bolivia',
                    'BW' => 'Botswana',
                    'BR' => 'Brazil',
                    'BN' => 'Brunei',
                    'BG' => 'Bulgaria',
                    'BF' => 'Burkina-faso',
                    'MM' => 'Burma',
                    'BI' => 'Burundi',
                    'CM' => 'Cameroon',
                    'CA' => 'Canada',
                    'CF' => 'Central African Republic',
                    'TD' => 'Chad',
                    'CL' => 'Chile',
                    'CN' => 'China',
                    'CO' => 'Colombia',
                    'CG' => 'Congo',
                    'CK' => 'Cook Is.',
                    'CR' => 'Costa Rica',
                    'CU' => 'Cuba',
                    'CY' => 'Cyprus',
                    'CZ' => 'Czech Republic',
                    'DK' => 'Denmark',
                    'DJ' => 'Djibouti',
                    'DO' => 'Dominica Rep.',
                    'EC' => 'Ecuador',
                    'EG' => 'Egypt',
                    'SV' => 'EI Salvador',
                    'EE' => 'Estonia',
                    'ET' => 'Ethiopia',
                    'FJ' => 'Fiji',
                    'FI' => 'Finland',
                    'FR' => 'France',
                    'GF' => 'French Guiana',
                    'GA' => 'Gabon',
                    'GM' => 'Gambia',
                    'GE' => 'Georgia',
                    'DE' => 'Germany',
                    'GH' => 'Ghana',
                    'GI' => 'Gibraltar',
                    'GR' => 'Greece',
                    'GD' => 'Grenada',
                    'GU' => 'Guam',
                    'GT' => 'Guatemala',
                    'GN' => 'Guinea',
                    'GY' => 'Guyana',
                    'HT' => 'Haiti',
                    'HN' => 'Honduras',
                    'HK' => 'Hong Kong',
                    'HU' => 'Hungary',
                    'IS' => 'Iceland',
                    'IN' => 'India',
                    'ID' => 'Indonesia',
                    'IR' => 'Iran',
                    'IQ' => 'Iraq',
                    'IE' => 'Ireland',
                    'IL' => 'Israel',
                    'IT' => 'Italy',
                    'JM' => 'Jamaica',
                    'JP' => 'Japan',
                    'JO' => 'Jordan',
                    'KH' => 'Kampuchea (Cambodia )',
                    'KZ' => 'Kazakstan',
                    'KE' => 'Kenya',
                    'KR' => 'Korea',
                    'KW' => 'Kuwait',
                    'KG' => 'Kyrgyzstan',
                    'LA' => 'Laos',
                    'LV' => 'Latvia',
                    'LB' => 'Lebanon',
                    'LS' => 'Lesotho',
                    'LR' => 'Liberia',
                    'LY' => 'Libya',
                    'LI' => 'Liechtenstein',
                    'LT' => 'Lithuania',
                    'LU' => 'Luxembourg',
                    'MO' => 'Macao',
                    'MG' => 'Madagascar',
                    'MW' => 'Malawi',
                    'MY' => 'Malaysia',
                    'MV' => 'Maldives',
                    'ML' => 'Mali',
                    'MT' => 'Malta',
                    'MU' => 'Mauritius',
                    'MX' => 'Mexico',
                    'MD' => 'Moldova, Republic of',
                    'MC' => 'Monaco',
                    'MN' => 'Mongolia',
                    'MS' => 'Montserrat Is',
                    'MA' => 'Morocco',
                    'MZ' => 'Mozambique',
                    'NA' => 'Namibia',
                    'NR' => 'Nauru',
                    'NP' => 'Nepal',
                    'NL' => 'Netherlands',
                    'NZ' => 'New Zealand',
                    'NI' => 'Nicaragua',
                    'NE' => 'Niger',
                    'NG' => 'Nigeria',
                    'KP' => 'North Korea',
                    'NO' => 'Norway',
                    'OM' => 'Oman',
                    'PK' => 'Pakistan',
                    'PA' => 'Panama',
                    'PG' => 'Papua New Cuinea',
                    'PY' => 'Paraguay',
                    'PE' => 'Peru',
                    'PH' => 'Philippines',
                    'PL' => 'Poland',
                    'PF' => 'French Polynesia',
                    'PT' => 'Portugal',
                    'PR' => 'Puerto Rico',
                    'QA' => 'Qatar',
                    'RO' => 'Romania',
                    'RU' => 'Russia',
                    'LC' => 'St.Lucia',
                    'VC' => 'St.Vincent',
                    'SM' => 'San Marino',
                    'ST' => 'Sao Tome and Principe',
                    'SA' => 'Saudi Arabia',
                    'SN' => 'Senegal',
                    'SC' => 'Seychelles',
                    'SL' => 'Sierra Leone',
                    'SG' => 'Singapore',
                    'SK' => 'Slovakia',
                    'SI' => 'Slovenia',
                    'SB' => 'Solomon Is',
                    'SO' => 'Somali',
                    'ZA' => 'South Africa',
                    'ES' => 'Spain',
                    'LK' => 'Sri Lanka',
                    'SD' => 'Sudan',
                    'SR' => 'Suriname',
                    'SZ' => 'Swaziland',
                    'SE' => 'Sweden',
                    'CH' => 'Switzerland',
                    'SY' => 'Syria',
                    'TW' => 'Taiwan',
                    'TJ' => 'Tajikstan',
                    'TZ' => 'Tanzania',
                    'TH' => 'Thailand',
                    'TG' => 'Togo',
                    'TO' => 'Tonga',
                    'TT' => 'Trinidad and Tobago',
                    'TN' => 'Tunisia',
                    'TR' => 'Turkey',
                    'TM' => 'Turkmenistan',
                    'TW' => 'Taiwan',
                    'UG' => 'Uganda',
                    'UA' => 'Ukraine',
                    'AE' => 'United Arab Emirates',
                    'GB' => 'United Kingdom',
                    'US' => 'United States',
                    'UY' => 'Uruguay',
                    'UZ' => 'Uzbekistan',
                    'VE' => 'Venezuela',
                    'VN' => 'Vietnam',
                    'YE' => 'Yemen',
                    'YU' => 'Yugoslavia',
                    'ZW' => 'Zimbabwe',
                    'ZR' => 'Zaire',
                    'ZM' => 'Zambia',
                );

                $skus = array();
                $sku_item = array(
                    "sku" => '',
                    "count" => 0,
                    "single_price" => 0,
                    "total_sales" => 0
                );
                $country = array();
                $cry_item = array(
                    "country" => '',
                    "count" => 0,
                    "total_sales" => 0
                );
                /*                $pay_method = array();
                                $pay_item = array(
                                    "method" => '',
                                    "count" => 0,
                                );*/
                $orderidHandle = fopen("/tmp/activity/statistics_pending_orderid.csv","r");
                $countryHandle = fopen("/tmp/activity/statistics_penging_country.csv","w");
                $proHandle = fopen("/tmp/activity/statistics_pengding_pro.log","w");
                //$paymethodHandle = fopen("/tmp/activity/paymethod.csv","w");
                $skuHandle = fopen("/tmp/activity/statistics_pengding_skulist.csv","w");
                while(!feof($orderidHandle)) {
                    $orderid[] = fgetcsv($orderidHandle);
                }
                fclose($orderidHandle);

                $idHandle = fopen("/tmp/activity/statistics_pending_orderid.log","w");
                fwrite($idHandle,var_export($orderid,true));
                fclose($idHandle);
                $sum_count = 0;
                if(isset($orderid) && !empty($orderid[0])) {
                    foreach($orderid as $oid) {
                        if(!empty($oid)) {
                            //for order
                            $result = $client->salesOrderInfo($session, $oid[0]);
                            $result = (array)$result;
                            //fwrite($proHandle,var_export($result,true));
                            //统计每个国家购买量
                            $countrys = (array)$result['shipping_address'];
                            if(isset($country[$countrys['country_id']])) {
                                $country[$countrys['country_id']]['count']++;
                                $country[$countrys['country_id']]['total_sales'] += $result['base_grand_total'];
                                $sum_count++;
                            } else {
                                $cry_item['country'] = $countrys_sx[$countrys['country_id']];
                                $cry_item['count'] ++;
                                $cry_item['total_sales'] = $result['base_grand_total'];
                                $country[$countrys['country_id']] = $cry_item;
                                $cry_item['country'] = '';
                                $cry_item['count'] = 0;
                                $cry_item['total_sales'] = 0;
                                $sum_count++;
                            }

                            //统计sku
                            foreach($result['items'] as $key => $product) {
                                $product = (array)$product;
                                //将产品剥离出来，具体一个sku下面对应一个产品
                                if($product['product_type'] == "configurable") {
                                    if(isset($skus[$product['sku']])) {
                                        $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                        $skus[$product['sku']]['count'] +=1;
                                        $skus[$product['sku']]['total_sales'] += $product['base_original_price']*$qty;
                                    } else {
                                        $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                        $sku_item['sku'] = $product['sku'];
                                        $sku_item['count'] +=1;
                                        $sku_item['single_price'] = $product['base_original_price'];
                                        $sku_item['total_sales'] = $product['base_original_price']*$qty;

                                        $skus[$product['sku']] = $sku_item;
                                    }
                                } else {
                                    $price = $product['price'];
                                    $price = (float)$price;
                                    $orginal_price = $product['original_price'];
                                    $orginal_price = (float)$orginal_price;
                                    if(isset($skus[$product['sku']]) && empty($price) && empty($orginal_price)) {  // configurable

                                    } else {
                                        if(isset($skus[$product['sku']])) {
                                            $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                            $skus[$product['sku']]['count'] +=1;
                                            $skus[$product['sku']]['total_sales'] += $product['base_original_price']*$qty;
                                        } else {
                                            $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                            $sku_item['sku'] = $product['sku'];
                                            $sku_item['count'] +=1;
                                            $sku_item['single_price'] = $product['base_original_price'];
                                            $sku_item['total_sales'] = $product['base_original_price']*$qty;

                                            $skus[$product['sku']] = $sku_item;
                                        }
                                    }
                                }

                                $sku_item['sku'] = '';
                                $sku_item['count'] = 0;
                                $sku_item['single_price'] = 0;
                                $sku_item['total_sales'] = 0;
                            }
                            $products = '';
                            $order_product = '';
                            unset($order_product);
                            unset($products);
                        }
                    }
                    foreach($skus as $sku) {
                        fputcsv($skuHandle,$sku);
                    }

                    foreach($country as $c) {
                        fputcsv($countryHandle,$c);
                    }

                    fclose($skuHandle);
                    fclose($countryHandle);
                    fwrite($proHandle,$sum_count);
                    fclose($proHandle);
                    unlink("/tmp/activity/statistics_pending_orderid.csv");
                    echo 1;
                }
            }
        }
    }

    /*
     * 用于单独查找用户的方法
     */
    public function singlecustomerAction() {
		$client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $session = $client->login('irulu', 'iruluapitest');
        $check_condition = 'email';
        $key = 'eq';
        $value = "ikurniawan@lacare.org";

        $complexFilter = array(
            'complex_filter' => array(
                array(
                    'key' => $check_condition,
                    'value' => array('key' => $key, 'value' => $value)  //筛选注册时间大于某个时间点的用户列表
                )
            )
        );

        $user_list = $client->customerCustomerList($session, $complexFilter); //获取用户列表

        $user_filter_log = fopen("/tmp/manual/user_filter.log","a+");
        fwrite($user_filter_log,var_export($complexFilter, true));
        fwrite($user_filter_log,var_export($user_list, true));
        fclose($user_filter_log);
        $handle = fopen("/tmp/manual/user_list.csv", "w");
        //$handle_log = fopen("/tmp/manual/user_list.log","w");

        foreach ($user_list as $user) {
            if(!empty($user)) {
                $user = (array)$user;
                $user['created_at'] = strtotime($user['created_at']);
                $user['updated_at'] = strtotime($user['updated_at']);
                if (isset($user['middlename'])) {
                    unset($user['middlename']);
                }
                fputcsv($handle, $user);
            }
        }
        //fwrite($handle_log,var_export($user,true));
        //fclose($handle_log);
        fclose($handle);
        $client->endSession($session);
    }


    public function singleuseraddressAction() {
        if(file_exists("/tmp/manual/userinfo.csv")) {
            unlink("/tmp/manual/userinfo.csv");
        }
        if(file_exists("/tmp/manual/user_list.csv")) {
            $client = new SoapClient('https://www.irulu.com/index.php/api/soap/?wsdl');
            $session = $client->login('irulu', 'iruluapitest');

            $handle = fopen("/tmp/manual/user_list.csv","r");

            while(!feof($handle)) {
                $content[] = fgetcsv($handle);
            }
            fclose($handle);
            $singleHandle = fopen("/tmp/manual/single.log","w");
            fwrite($singleHandle,var_export($content,true));
            if(!empty($content)) {
                $userHandle = fopen("/tmp/manual/userinfo.csv","w");

                foreach($content as $k => $user) {
                    if(!empty($user)) {
                        $userinfo[$k]['email'] = $user[6];
                        $userinfo[$k]['lastname'] = $user[8];
                        $userinfo[$k]['firstname'] = $user[7];
                        $userinfo[$k]['password'] = $user[10];
                        $userinfo[$k]['register_date'] = $user[1];
                        $result = $client->call($session, 'customer_address.list', $user[0]); //获取用户地址列表
                        $userinfo[$k]['post_addr'] = $result[0]['street'].','.$result[0]['city'].','.$result[0]['region'].','.$result[0]['postcode'].','.$result[0]['country_id'];
                        $userinfo[$k]['phone'] = $result[0]['telephone'];

                        //将$userinfo数据写入到csv文件中，然后再通过另一个方法读取该文件并将数据通过调用旧网站的api写入旧的数据库
                        fputcsv($userHandle,$userinfo[$k]);
                    }
                }
                fclose($userHandle);
                fclose($singleHandle);  ///tmp/users/userinfo.csv
				//将 /tmp/manual/userinfo.csv 文件拷贝到 tmp/users/userinfo.csv 目录，然后删除/tmp/manual/下的userinfo.csv文件
				$oldDir = "/tmp/manual/userinfo.csv";
				$newDir = "/tmp/users/userinfo.csv";
				$result = copy(oldDir,$newDir);
				if($result) {
					unlink($oldDir);
				}
				
                unlink("/tmp/manual/user_list.csv");
                $client->endSession($session);
                echo 1;
            }
        }
    }

    //将因为没有用户而导致的订单没有正常导入的单个订单列表获取出来
    public function singleorderlistAction() {
        date_default_timezone_set("Asia/Shanghai");
        $weekday = date("w",time());
        switch($weekday) {
            case 0: $action = 1;break;
            case 6: $action = 1;break;
            default:$action = 1;break;
        }

        //读取没有成功录入irulu数据库的用户数据
		$fail_get_uid_dir = "/tmp/paidorder/fail_get_uid.csv";

			$orderidHandle = fopen("/tmp/manual/manual_orderid.csv", "w");
			
            //if(file_exists("/tmp/manual/manual_orderid.csv")) {
            //    unlink("/tmp/manual/manual_orderid.csv");
            //}
			
            $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
            $session = $client->login('irulu', 'iruluapitest');

				$filter = array(
                /*'filter' => array(   //固定筛选某个
                    array('key' => 'status', 'value' => 'processing'),
                ),*/
                'complex_filter' => array(
                    array(
                        'key' => 'increment_id',
                        'value' => array(
                            'key' => 'eq',
                            'value' => "1171122221458330"//orderid
							),
						),
					)
				);
				$order_list = $client->salesOrderList($session,$filter);
				$orderHandle = fopen("/tmp/manual/manual_orders.txt","w");
				fwrite($orderHandle,var_export($order_list,true));
				fclose($orderHandle);
				if(!empty($order_list)) {
					
					//用于备份，如果这天的订单被导出但是忘记确认那么就使用这个备份重新确认，因为processing_orderid.csv在每天的更新中会被覆盖

					foreach($order_list as $ok => $or) {
						$or = (array)$or;
						$orderid[0]=$or['increment_id'];
						fputcsv($orderidHandle,$orderid);
					}
					
				}
			fclose($orderidHandle);
			//将/tmp/manual/目录下的manual_orderid.csv文件复制到 /tmp/paidorder/目录下并改名为orderid.csv
			$oldDir = "/tmp/manual/manual_orderid.csv";
			$newDir = "/tmp/paidorder/orderid.csv";
			$result = copy(oldDir,$newDir);
			if($result) {
				unlink($oldDir);
				unlink($fail_get_uid_dir);
			}
		
		
    }

    /*
     * 方法说明：此方法用于那些没有通过网站注册直接使用paypal购买商品的用户，邮箱使用paypal邮箱
     */
    public function orderdetailforpaypalAction() {
        date_default_timezone_set("Asia/Shanghai");
        $weekday = date("w",time());

        switch($weekday) {
            case 0: $action = 0;break;
            case 6: $action = 0;break;
            default:$action = 1;break;
        }

        if($action) {
            if(file_exists("/tmp/manual/manual_orderdetail.csv")) {
                unlink("/tmp/manual/manual_orderdetail.csv");
            }

            if(file_exists("/tmp/manual/manual_orderid.csv")) {
                $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
                $session = $client->login('irulu', 'iruluapitest');
                $paytimeHandle = fopen("/tmp/manual/manual_paytime.txt","w");
                $ut = fopen("/tmp/manual/manual_ut.log","w");
                $countrys = array (
                    'AO' => 'Angola',
                    'AF' => 'Afghanistan',
                    'AL' => 'Albania',
                    'DZ' => 'Algeria',
                    'AD' => 'Andorra',
                    'AI' => 'Anguilla',
                    'AG' => 'Antigua and Barbuda',
                    'AR' => 'Argentina',
                    'AM' => 'Armenia',
                    'AU' => 'Australia',
                    'AT' => 'Austria',
                    'AZ' => 'Azerbaijan',
                    'BS' => 'Bahamas',
                    'BH' => 'Bahrain',
                    'BD' => 'Bangladesh',
                    'BB' => 'Barbados',
                    'BY' => 'Belarus',
                    'BE' => 'Belgium',
                    'BZ' => 'Belize',
                    'BJ' => 'Benin',
                    'BM' => 'Bermuda Is.',
                    'BO' => 'Bolivia',
                    'BW' => 'Botswana',
                    'BR' => 'Brazil',
                    'BN' => 'Brunei',
                    'BG' => 'Bulgaria',
                    'BF' => 'Burkina-faso',
                    'MM' => 'Burma',
                    'BI' => 'Burundi',
                    'CM' => 'Cameroon',
                    'CA' => 'Canada',
                    'CF' => 'Central African Republic',
                    'TD' => 'Chad',
                    'CL' => 'Chile',
                    'CN' => 'China',
                    'CO' => 'Colombia',
                    'CG' => 'Congo',
                    'CK' => 'Cook Is.',
                    'CR' => 'Costa Rica',
                    'CU' => 'Cuba',
                    'CY' => 'Cyprus',
                    'CZ' => 'Czech Republic',
                    'DK' => 'Denmark',
                    'DJ' => 'Djibouti',
                    'DO' => 'Dominica Rep.',
                    'EC' => 'Ecuador',
                    'EG' => 'Egypt',
                    'SV' => 'EI Salvador',
                    'EE' => 'Estonia',
                    'ET' => 'Ethiopia',
                    'FJ' => 'Fiji',
                    'FI' => 'Finland',
                    'FR' => 'France',
                    'GF' => 'French Guiana',
                    'GA' => 'Gabon',
                    'GM' => 'Gambia',
                    'GE' => 'Georgia',
                    'DE' => 'Germany',
                    'GH' => 'Ghana',
                    'GI' => 'Gibraltar',
                    'GR' => 'Greece',
                    'GD' => 'Grenada',
                    'GU' => 'Guam',
                    'GT' => 'Guatemala',
                    'GN' => 'Guinea',
                    'GY' => 'Guyana',
                    'HT' => 'Haiti',
                    'HN' => 'Honduras',
                    'HK' => 'Hong Kong',
                    'HU' => 'Hungary',
                    'IS' => 'Iceland',
                    'IN' => 'India',
                    'ID' => 'Indonesia',
                    'IR' => 'Iran',
                    'IQ' => 'Iraq',
                    'IE' => 'Ireland',
                    'IL' => 'Israel',
                    'IT' => 'Italy',
                    'JM' => 'Jamaica',
                    'JP' => 'Japan',
                    'JO' => 'Jordan',
                    'KH' => 'Kampuchea (Cambodia )',
                    'KZ' => 'Kazakstan',
                    'KE' => 'Kenya',
                    'KR' => 'Korea',
                    'KW' => 'Kuwait',
                    'KG' => 'Kyrgyzstan',
                    'LA' => 'Laos',
                    'LV' => 'Latvia',
                    'LB' => 'Lebanon',
                    'LS' => 'Lesotho',
                    'LR' => 'Liberia',
                    'LY' => 'Libya',
                    'LI' => 'Liechtenstein',
                    'LT' => 'Lithuania',
                    'LU' => 'Luxembourg',
                    'MO' => 'Macao',
                    'MG' => 'Madagascar',
                    'MW' => 'Malawi',
                    'MY' => 'Malaysia',
                    'MV' => 'Maldives',
                    'ML' => 'Mali',
                    'MT' => 'Malta',
                    'MU' => 'Mauritius',
                    'MX' => 'Mexico',
                    'MD' => 'Moldova, Republic of',
                    'MC' => 'Monaco',
                    'MN' => 'Mongolia',
                    'MS' => 'Montserrat Is',
                    'MA' => 'Morocco',
                    'MZ' => 'Mozambique',
                    'NA' => 'Namibia',
                    'NR' => 'Nauru',
                    'NP' => 'Nepal',
                    'NL' => 'Netherlands',
                    'NZ' => 'New Zealand',
                    'NI' => 'Nicaragua',
                    'NE' => 'Niger',
                    'NG' => 'Nigeria',
                    'KP' => 'North Korea',
                    'NO' => 'Norway',
                    'OM' => 'Oman',
                    'PK' => 'Pakistan',
                    'PA' => 'Panama',
                    'PG' => 'Papua New Cuinea',
                    'PY' => 'Paraguay',
                    'PE' => 'Peru',
                    'PH' => 'Philippines',
                    'PL' => 'Poland',
                    'PF' => 'French Polynesia',
                    'PT' => 'Portugal',
                    'PR' => 'Puerto Rico',
                    'QA' => 'Qatar',
                    'RO' => 'Romania',
                    'RU' => 'Russia',
                    'LC' => 'St.Lucia',
                    'VC' => 'St.Vincent',
                    'SM' => 'San Marino',
                    'ST' => 'Sao Tome and Principe',
                    'SA' => 'Saudi Arabia',
                    'SN' => 'Senegal',
                    'SC' => 'Seychelles',
                    'SL' => 'Sierra Leone',
                    'SG' => 'Singapore',
                    'SK' => 'Slovakia',
                    'SI' => 'Slovenia',
                    'SB' => 'Solomon Is',
                    'SO' => 'Somali',
                    'ZA' => 'South Africa',
                    'ES' => 'Spain',
                    'LK' => 'Sri Lanka',
                    'SD' => 'Sudan',
                    'SR' => 'Suriname',
                    'SZ' => 'Swaziland',
                    'SE' => 'Sweden',
                    'CH' => 'Switzerland',
                    'SY' => 'Syria',
                    'TW' => 'Taiwan',
                    'TJ' => 'Tajikstan',
                    'TZ' => 'Tanzania',
                    'TH' => 'Thailand',
                    'TG' => 'Togo',
                    'TO' => 'Tonga',
                    'TT' => 'Trinidad and Tobago',
                    'TN' => 'Tunisia',
                    'TR' => 'Turkey',
                    'TM' => 'Turkmenistan',
                    'TW' => 'Taiwan',
                    'UG' => 'Uganda',
                    'UA' => 'Ukraine',
                    'AE' => 'United Arab Emirates',
                    'GB' => 'United Kingdom',
                    'US' => 'United States',
                    'UY' => 'Uruguay',
                    'UZ' => 'Uzbekistan',
                    'VE' => 'Venezuela',
                    'VN' => 'Vietnam',
                    'YE' => 'Yemen',
                    'YU' => 'Yugoslavia',
                    'ZW' => 'Zimbabwe',
                    'ZR' => 'Zaire',
                    'ZM' => 'Zambia',
                );

                $userListHandle = fopen("/tmp/manual/user_list.csv","r");
                while(!feof($userListHandle)) {
                    $userlist[] = fgetcsv($userListHandle);
                }
                fwrite($ut,var_export($userlist,true));
                fclose($ut);
                $orderidHandle = fopen("/tmp/manual/manual_orderid.csv","r");
                $proHandle = fopen("/tmp/manual/manual_pro.txt","w");
                while(!feof($orderidHandle)) {
                    $orderid[] = fgetcsv($orderidHandle);
                }
                fclose($orderidHandle);

                $idHandle = fopen("/tmp/manual/manual_order_id_txt.txt","w");
                fwrite($idHandle,var_export($orderid,true));
                fclose($idHandle);

                if(isset($orderid) && !empty($orderid[0])) {
                    $orderDetailHandle = fopen("/tmp/manual/manual_orderdetail.csv","w");
                    $orderDetailLogHandle = fopen("/tmp/manual/manual_order_detail_log.txt","w");
                    foreach($orderid as $oid) {
                        if(!empty($oid)) {
                            //for order
                            $result = $client->salesOrderInfo($session, $oid[0]);
                            $result = (array)$result;
                            $order_detail['order_id'] = $result['increment_id'];
                            $order_detail['create_time'] = strtotime($result['created_at']);
                            $order_detail['email'] = $userlist[0][6];
                            $order_detail['shipping_fee'] = $result['shipping_amount'];
                            $order_detail['total_money'] = $result['base_grand_total'];
                            $order_detail['ip'] = isset($result['remote_ip']) ? sprintf("%u", ip2long($result['remote_ip'])) : "1883904612";  //ip需要转换为数字
                            $order_detail['status'] = $result['status'];
                            $order_detail['update_time'] = strtotime($result['updated_at']);
                            $paymethod = (array)$result['payment'];
                            $order_detail['pay_type'] = $paymethod['method'];
                            $userinfo = (array)$result['shipping_address'];
                            $firstname = $userinfo['firstname']." first name";
                            $lastname = $userinfo['lastname']." last name";
                            $order_detail['first_name'] = $firstname;
                            $order_detail['last_name'] = $lastname;
                            $order_detail['phone'] = $userinfo['telephone'];
                            $order_detail['zip_code'] = $userinfo['postcode'];
                            $order_detail['abbreviation'] = $userinfo['country_id'];
                            $order_detail['country'] = $countrys[$userinfo['country_id']];  //国家全称
                            $state = $userinfo['region']." user region";
                            $city = $userinfo['city'] . " user city";
                            $order_detail['state'] = $state;
                            $order_detail['city'] = $city;
                            //$order_detail['address1'] = $userinfo['street'].','.$userinfo['city'].','.$userinfo['region'].','.$userinfo['postcode'].','.$order_detail['country'];
                            $order_detail['address1'] = $userinfo['street'];

                            foreach($result['status_history'] as $paytypes) {
                                $paytypes = (array)$paytypes;
                                if($paytypes['status'] == 'processing') {
                                    fwrite($paytimeHandle,$paytypes['created_at']);
                                    $order_detail['pay_time'] = strtotime($paytypes['created_at']); //支付时间
                                    break;
                                }

                                /*                        foreach($paytypes as $paytype) {
                                                            if($paytype['status'] == 'processing') {
                                                                fwrite($paytimeHandle,$paytype['created_at']);
                                                                $order_detail['pay_time'] = strtotime($paytype['created_at']); //支付时间
                                                                break;
                                                            }
                                                        }*/
                            }

                            $products = array();
                            $order_product = array();
                            //for orderdetail
                            foreach($result['items'] as $key => $product) {
                                $product = (array)$product;
                                //将产品剥离出来，具体一个sku下面对应一个产品
                                if($product['product_type'] == "configurable") {
                                    $items = array('configurable' => '','simple' => '');
                                    $items['configurable'] = $product;
                                    $products[$product['sku']] = $items;
                                    $items = '';
                                    unset($items);
                                } else {
                                    $price = $product['price'];
                                    $price = (float)$price;
                                    $orginal_price = $product['original_price'];
                                    $orginal_price = (float)$orginal_price;
                                    if(isset($products[$product['sku']]) && empty($price) && empty($orginal_price)) {  // configurable
                                        $products[$product['sku']]['simple'] = $product;
                                        $products[$product['sku']]['configurable']['name'] = $products[$product['sku']]['simple']['name'];
                                        $products[$product['sku']]['configurable']['product_id'] = $products[$product['sku']]['simple']['product_id'];
                                        //fwrite($empHandle,var_export($products[$product['sku']]['configurable'],true));
                                        //fwrite($empHandle,"\n");
                                        $tmp = $products[$product['sku']]['configurable'];
                                        $products[$product['sku']]['configurable'] = '';
                                        $products[$product['sku']]['simple'] = '';
                                        unset($products[$product['sku']]['configurable']);
                                        unset($products[$product['sku']]['simple']);
                                        //fwrite($empHandle,"tmp");
                                        //fwrite($empHandle,"\n");
                                        //fwrite($empHandle,var_export($tmp,true));
                                        //$products[$product['sku']] = $tmp;
                                        $order_product[] = $tmp;
                                        $tmp = '';
                                        unset($tmp);
                                    } else {
                                        //$products[$product['sku']] = $product;
                                        //fwrite($empHandle,"\n");
                                        $order_product[] = $product;
                                    }
                                }

                                foreach($order_product as $pk => $pdt) {
                                    $product_item[$pk]['product_id'] = $pdt['product_id'];
                                    $product_item[$pk]['order_id'] = $pdt['order_id'];
                                    $product_item[$pk]['sku_id'] = 0;
                                    $product_item[$pk]['uniqid'] = $pdt['sku'];
                                    $qty = ceil($pdt['qty_ordered']) - ceil($pdt['qty_canceled']);
                                    $product_item[$pk]['total_money'] = $pdt['base_original_price']*$qty;
                                    $product_item[$pk]['pay_money'] = $pdt['base_original_price']*$qty;
                                    $product_item[$pk]['original_price'] = $pdt['original_price'];
                                    $product_item[$pk]['price'] = $pdt['base_original_price'];
                                    $product_item[$pk]['quantity'] = $qty;
                                    $product_item[$pk]['product_name'] = $pdt['name'];
                                    $product_item[$pk]['product_sku'] = $pdt['sku'];
                                    //$product_item[$pk]['product_type'] = $pdt['product_type'];
                                }
                            }
                            $order_detail['items'] = serialize($product_item);
                            $product_item = "";
                            unset($product_item);
                            fputcsv($orderDetailHandle,$order_detail);
                            fwrite($proHandle,var_export($result,true));
                            $products = '';
                            $order_product = '';
                            unset($order_product);
                            unset($products);
                        }/* else {
                $order_detail['items'] = "empty";
                $order_detail['oid'] = $oid[0];

                fputcsv($orderDetailHandle,$order_detail);
            }*/

                        //write log
                        fwrite($orderDetailLogHandle,$oid);
                        fwrite($orderDetailLogHandle,"\n");
                        fwrite($orderDetailLogHandle,var_export($order_detail,true));
                    }
                    fclose($orderDetailHandle);
                    fclose($orderDetailLogHandle);
                    fclose($proHandle);
                    fclose($paytimeHandle);
                    fclose($userListHandle);
                    unlink("/tmp/manual/manual_orderid.csv");
                    echo 1;
                }
            }
        }
    }


    /*
     * 方法说明：统计sku 财务要的
     */
    public function statisticsorderskuforcaiwuAction() {
        date_default_timezone_set("Asia/Shanghai");
        $weekday = date("w",time());

        switch($weekday) {
            case 0: $action = 1;break;
            case 6: $action = 1;break;
            default:$action = 1;break;
        }

        if($action) {
            if(file_exists("/tmp/activity/statistics_caiwu_orderdetail.csv")) {
                unlink("/tmp/activity/statistics_caiwu_orderdetail.csv");
            }

            if(file_exists("/tmp/activity/statistics_caiwu_orderid.csv")) {
                $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
                $session = $client->login('irulu', 'iruluapitest');

                $skus = array();
                $sku_item = array(
                    "order_id" => '',
                    "sku" => '',
                    "count" => 0,
                    //"single_price" => 0,
                    //"total_sales" => 0
                );

                $pay_method = array();
                $pay_item = array(
                    "method" => '',
                    "count" => 0,
                );
                $orderidHandle = fopen("/tmp/activity/statistics_caiwu_orderid.csv","r");
                $countryHandle = fopen("/tmp/activity/statistics_country.csv","w");
                $proHandle = fopen("/tmp/activity/statistics_pro.log","w");
                $pm = fopen("/tmp/activity/pm.txt","w");
                $skucaiwu = fopen("/tmp/activity/skucaiwu.txt","w");
                $paymethodHandle = fopen("/tmp/activity/paymethod.csv","w");
                $skuHandle = fopen("/tmp/activity/skulist.csv","w");
                while(!feof($orderidHandle)) {
                    $orderid[] = fgetcsv($orderidHandle);
                }
                fclose($orderidHandle);

                unset($orderid[0]);
                for($u = 222;$u <= 233;$u++) {
                    unset($orderid[$u]);
                }

                $idHandle = fopen("/tmp/activity/statistics_caiwu_orderid.txt","w");
                fwrite($idHandle,var_export($orderid,true));
                fclose($idHandle);
                $sum_count = 0;
                if(isset($orderid) && !empty($orderid[1])) {
                    foreach($orderid as $oid) {
                        if(!empty($oid[1])) {
                            //for order
                            $result = $client->salesOrderInfo($session, $oid[1]);

                            if(!empty($result)) {
                                $result = (array)$result;

                                //统计sku
                                foreach($result['items'] as $key => $product) {
                                    $product = (array)$product;

                                    //将产品剥离出来，具体一个sku下面对应一个产品
                                    if($product['product_type'] == "configurable") {
                                        if(isset($skus[$result['increment_id']])) {
                                            $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                            $skus[$result['increment_id']]['count'] +=1;
                                            //$skus[$product['sku']]['total_sales'] += $product['base_original_price']*$qty;
                                        } else {
                                            $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                            $sku_item['sku'] = $product['sku'];
                                            $sku_item['count'] +=1;
                                            //$sku_item['single_price'] = $product['base_original_price'];
                                            //$sku_item['total_sales'] = $product['base_original_price']*$qty;
                                            $sku_item['order_id'] = $result['increment_id'];
                                            $skus[$result['increment_id']] = $sku_item;
                                        }
                                    } else {
                                        $price = $product['price'];
                                        $price = (float)$price;
                                        $orginal_price = $product['original_price'];
                                        $orginal_price = (float)$orginal_price;
                                        if(isset($skus[$result['increment_id']]) && empty($price) && empty($orginal_price)) {  // configurable

                                        } else {
                                            if(isset($skus[$result['increment_id']])) {
                                                $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                                $skus[$result['increment_id']]['count'] +=1;
                                                //$skus[$product['sku']]['total_sales'] += $product['base_original_price']*$qty;
                                            } else {
                                                $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                                                $sku_item['sku'] = $product['sku'];
                                                $sku_item['count'] +=1;
                                                $sku_item['order_id'] = $result['increment_id'];
                                                //$sku_item['single_price'] = $product['base_original_price'];
                                                //$sku_item['total_sales'] = $product['base_original_price']*$qty;

                                                $skus[$result['increment_id']] = $sku_item;
                                            }
                                        }
                                    }

                                    $sku_item['sku'] = '';
                                    $sku_item['count'] = 0;
                                    //$sku_item['single_price'] = 0;
                                    //$sku_item['total_sales'] = 0;
                                }
                                $products = '';
                                $order_product = '';
                                unset($order_product);
                                unset($products);
                            }
                        }
                    }
                    fwrite($skucaiwu,var_export($result,true));
                    foreach($skus as $sku) {
                        fputcsv($skuHandle,$sku);
                    }


                    foreach($pay_method as $p) {
                        fputcsv($paymethodHandle,$p);
                    }
                    fclose($skuHandle);
                    fclose($countryHandle);
                    fclose($paymethodHandle);
                    fclose($pm);
                    fclose($skucaiwu);
                    fwrite($proHandle,$sum_count);
                    fclose($proHandle);
                    unlink("/tmp/activity/statistics_caiwu_orderid.csv");
                    echo 1;
                }
            }
        }
    }
}