<?php
/**
 * Created by PhpStorm.
 * User: jacki_hu
 * Date: 2017/5/12
 * Time: 22:08
 */

class Helloworld_Wyd_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Index action
     */
    public function indexAction()
    {
        //$client = new SoapClient('http://112.74.26.100:8088/index.php/api/soap/?wsdl');
        $client = new SoapClient('http://112.74.26.100:8088/index.php/api/v2_soap/?wsdl');
        //$session = $client->login('irulu', 'iruluapitest');
        $session = $client->login('jacki', 'qwert12345');
        $complexFilter = array(
            'complex_filter' => array(
                array(
                    'key' => 'created_at',
                    //'value' => array('key' => 'gt', 'value' => '2017-05-24 23:24:51')  //筛选注册时间大于某个时间点的用户列表
                    'value' => array('key' => 'eq', 'value' => '2013-05-16 00:45:10')  //筛选注册时间大于某个时间点的用户列表
                )
            )
        );

        //以下两个接口是用于获取用户列表和具体用户的地址信息
        //$user_list = $client->call($session, 'customer.list',$complexFilter); //获取用户列表
        $user_list = $client->customerCustomerList($session, $complexFilter); //获取用户列表
        //$user_list = $client->customerCustomerList($session); //获取用户列表

        $handle = fopen("user_list.csv","w");
            $user = (array)$user_list[0];
            $user['created_at'] = strtotime($user['created_at']);
            $user['updated_at'] = strtotime($user['updated_at']);
            fputcsv($handle,$user);
        fclose($handle);



        //将用户列表写入文件中，因为magento的api获取时间交长，为了防止在操作过程中连接中断造成错误
/*        $handle = fopen("user_list.csv","w");
        foreach($user_list as $user) {
            $user = (array)$user;
            $user['created_at'] = strtotime($user['created_at']);
            $user['updated_at'] = strtotime($user['updated_at']);
            fputcsv($handle,$user);
        }
        fclose($handle);*/

        echo "<pre>";
        print_r($user_list);
/*        foreach($user_list as $user) {
            //$result = $client->call($session, 'customer_address.list', $user['customer_id']); //获取用户地址列表
            $user = (array)$user;
            $result = $client->customerAddressList($session, $user['customer_id']); //获取用户地址列表
            echo "<pre>";
            print_r($result);
        }*/
    }

    /*
     * 方法说明：获取用户列表
     * 调用方法：http://domain/index.php/account/index/userlist
     */
    public function userlistAction() {
/*        $client = new SoapClient('http://112.74.26.100:8088/index.php/api/v2_soap/?wsdl');
        //$session = $client->login('irulu', 'iruluapitest');
        $session = $client->login('jacki', 'qwert12345');
        $complexFilter = array(
            'complex_filter' => array(
                array(
                    'key' => 'created_at',
                    'value' => array('key' => 'gt', 'value' => '2013-01-24 23:24:51')  //筛选注册时间大于某个时间点的用户列表
                )
            )
        );

        $user_list = $client->customerCustomerList($session, $complexFilter); //获取用户列表

        //这里的每个方法都需要判断一下从数据库中是否有获取到数据，如果有才创建文件，如果没有就不要创建，并且不要继续往下执行用户注册，但是订单与用户注册要分开
        //将用户列表写入文件中，因为magento的api获取时间交长，为了防止在操作过程中连接中断造成错误
        $handle = fopen("/tmp/user_list.csv","w");
        foreach($user_list as $user) {
              $user = (array)$user;
              $user['created_at'] = strtotime($user['created_at']);
              $user['updated_at'] = strtotime($user['updated_at']);
              fputcsv($handle,$user);
        }
        fclose($handle);*/
        echo 1;
    }

    /*
     * 方法说明：用户地址获取
     * 调用方法：http://domain/index.php/account/index/useraddress
     */
    public function useraddressAction() {
        //$client = new SoapClient('http://112.74.26.100:8088/index.php/api/v2_soap/?wsdl');
/*        $client = new SoapClient('http://112.74.26.100:8088/index.php/api/soap/?wsdl');
        $session = $client->login('jacki', 'qwert12345');


        $handle = fopen("/tmp/user_list.csv","r");

        while(!feof($handle)) {
            $content[] = fgetcsv($handle);
        }
        fclose($handle);

        $userHandle = fopen("/tmp/userinfo.csv","w");

        foreach($content as $k => $user) {
            if(!empty($user)) {
                $userinfo[$k]['email'] = $user[6];
                $userinfo[$k]['lastname'] = $user[8];
                $userinfo[$k]['firstname'] = $user[7];
                $userinfo[$k]['password'] = $user[11];
                $userinfo[$k]['register_date'] = $user[1];
                $result = $client->call($session, 'customer_address.list', $user[0]); //获取用户地址列表
                $userinfo[$k]['post_addr'] = $result[0]['street'].','.$result[0]['city'].','.$result[0]['region'].','.$result[0]['postcode'].','.$result[0]['country_id'];
                $userinfo[$k]['phone'] = $result[0]['telephone'];

                //将$userinfo数据写入到csv文件中，然后再通过另一个方法读取该文件并将数据通过调用旧网站的api写入旧的数据库
                fputcsv($userHandle,$userinfo[$k]);
            }
        }
        fclose($userHandle);*/
        echo 2;
    }

    /*
     * 方法说明：获取所有订单列表
     *调用方法：http://domain/index.php/account/index/orderlist
     */
    public function orderlistAction() {
/*        $client = new SoapClient('http://112.74.26.100:8088/index.php/api/v2_soap/?wsdl');
        $session = $client->login('jacki', 'qwert12345');
        $orderidHandle = fopen("/tmp/orderid.csv","w");
        $filter = array('complex_filter' => array(array('key' => 'created_at', 'value' => array('key' => 'gt','value'=>'2012-03-15 08:01:34'))));
        $order_list = $client->salesOrderList($session,$filter);
        foreach($order_list as $order) {
            $order = (array)$order;
            $orderid[0]=$order['increment_id'];
            fputcsv($orderidHandle,$orderid);
        }
        fclose($orderidHandle);*/
        echo 3;
    }

    /*
     * 方法说明：获取订单详情
     */
    public function orderdetailAction() {
        /*$orderidHandle = fopen("/tmp/orderid.csv","r");
        $client = new SoapClient('http://112.74.26.100:8088/index.php/api/v2_soap/?wsdl');
        $session = $client->login('jacki', 'qwert12345');

        $countrys = array (
            'AE' => 'UNITED EMIRATES',
            'AF' => 'AFGHANISTAN',
            'AL' => 'ALBANIA',
            'AM' => 'ARMENIA',
            'AO' => 'ANGOLA',
            'AR' => 'ARGENTINA',
            'AT' => 'AUSTRIA',
            'AU' => 'AUSTRALIA',
            'AZ' => 'AZERBAIJAN(REPUBLIC',
            'BD' => 'BANGLADESH',
            'BE' => 'BELGIUM',
            'BF' => 'BURKINA FASO',
            'BG' => 'BULGARIA',
            'BH' => 'BAHREIN',
            'BI' => 'BURUNDI',
            'BJ' => 'BENIN',
            'BN' => 'BRUNEI DARUSSALAM',
            'BO' => 'BOLIVIA',
            'BR' => 'BRAZIL',
            'BW' => 'BOTSWANA',
            'BY' => 'BYELORUSSIA',
            'CA' => 'CANADA',
            'CF' => 'CENTRAL AFRICA',
            'CG' => 'CONGO',
            'CH' => 'SWITZERLAND',
            'CL' => 'CHILE',
            'CM' => 'CAMEROON',
            'CN' => 'CHINA',
            'CO' => 'COLOMBIA',
            'CR' => 'COSTA RICA',
            'CS' => 'CZECH REPUBIC',
            'CU' => 'CUBA',
            'CY' => 'CYPRUS',
            'DE' => 'GERMANY',
            'DK' => 'DENMARK',
            'DO' => 'DOMINICAN REPUBLIC',
            'DZ' => 'ALGERIA',
            'EC' => 'ECUADOR',
            'EE' => 'ESTONIA',
            'EG' => 'EGYPT',
            'ES' => 'SPAIN',
            'ET' => 'ETHIOPIA',
            'FI' => 'FINLAND',
            'FJ' => 'FIJI',
            'FR' => 'FRANCE',
            'GA' => 'GABON',
            'GB' => 'UNITED KINGDOM',
            'GD' => 'GRENADA',
            'GE' => 'GEORGIA',
            'GH' => 'GHANA',
            'GN' => 'GUINEA',
            'GR' => 'GREECE',
            'GT' => 'GUATEMALA',
            'HK' => 'HONG KONG',
            'HN' => 'HONDURAS',
            'HU' => 'HUNGARY',
            'ID' => 'INDONESIA',
            'IE' => 'IRELAND',
            'IL' => 'ISRAEL',
            'IN' => 'INDIA',
            'IQ' => 'IRAQ',
            'IR' => 'IRAN',
            'IS' => 'ICELAND',
            'IT' => 'ITALY',
            'JM' => 'JAMAICA',
            'JO' => 'JORDAN',
            'JP' => 'JAPAN',
            'KG' => 'KYRGYZSTAN',
            'KH' => 'KAMPUCHEA(CAMBODIA',
            'KP' => 'KOREA,DEM.PEOPLE\'S',
            'KR' => 'REPUBLIC OF KOREA',
            'KT' => 'COTE O\'IVOIRE',
            'KW' => 'KUWATI',
            'KZ' => 'KAZAKHSTAN',
            'LA' => 'LAOS',
            'LB' => 'LEBANON',
            'LC' => 'SAINT LUEIA',
            'LI' => 'LIECHTENSTEIN',
            'LK' => 'SRI LANKA',
            'LR' => 'LIBERIA',
            'LT' => 'LITHUANIA',
            'LU' => 'LUXEMBOURG',
            'LV' => 'LATVIA',
            'LY' => 'LIBYAN',
            'MA' => 'MOROCCO',
            'MC' => 'MONACO',
            'MD' => 'MOLDOVA,REPUBLIC OF',
            'MG' => 'MADAGASCAR',
            'ML' => 'MALI',
            'MM' => 'BURMA(MYANMAR',
            'MN' => 'MONGOLIA',
            'MO' => 'MACAU',
            'MT' => 'MALTA',
            'MU' => 'MAURITIUS',
            'MW' => 'MALAWI',
            'MX' => 'MEXICO',
            'MY' => 'MALAYSIA',
            'MZ' => 'MOZAMBIQUE',
            'NA' => 'NAMIBIA',
            'NE' => 'NIGER',
            'NG' => 'NIGERIA',
            'NI' => 'NICARAGUA',
            'NL' => 'NETHERLANDS',
            'NO' => 'NORWAY',
            'NP' => 'NEPAL',
            'NZ' => 'NEW ZEALAND',
            'OM' => 'OMAN',
            'PA' => 'PANAMA',
            'PE' => 'PERU',
            'PG' => 'PAPUA NEW GUINEA',
            'PH' => 'PHILIPPINES',
            'PK' => 'PAKISTAN',
            'PL' => 'POLAND',
            'PT' => 'PORTUGAL',
            'PY' => 'PARAGUAY',
            'QA' => 'QATAR',
            'RO' => 'ROMANIA',
            'RU' => 'RUSSIAN FEDERATION',
            'SA' => 'SAUDI ARABIA',
            'SC' => 'SEYCHELLES',
            'SD' => 'SUDAN',
            'SE' => 'SWEDEN',
            'SG' => 'SINGAPORE',
            'SI' => 'SLOVENIA',
            'SK' => 'SLOVAKIA',
            'SM' => 'SAN MARINO',
            'SN' => 'SENEGAL',
            'SO' => 'SOMALIA',
            'SY' => 'SYRIA',
            'SZ' => 'SWAZILAND',
            'TD' => 'CHAD',
            'TG' => 'TOGO',
            'TH' => 'THAILAND',
            'TJ' => 'TAJIKISTAN',
            'TM' => 'TURKMENISTAN',
            'TN' => 'TUNISIA',
            'TR' => 'TURKEY',
            'TW' => 'TAIWAN',
            'TZ' => 'TANZANIA',
            'UA' => 'UKRAINE',
            'UG' => 'UGANDA',
            'US' => 'UNITED STATES',
            'UY' => 'URUGUAY',
            'UZ' => 'UZBEKISTAN',
            'VC' => 'SAINT VINCENT',
            'VE' => 'VENEZUELA',
            'VN' => 'VIET NAM',
            'YE' => 'YEMEN',
            'YU' => 'YUGOSLAVIA',
            'ZA' => 'SOUTH AFRICA',
            'ZM' => 'ZAMBIA',
            'ZR' => 'ZAIRE',
            'ZW' => 'ZIMBABWE',
        );


        while(!feof($orderidHandle)) {
            $orderid[] = fgetcsv($orderidHandle);
        }
        fclose($orderidHandle);

        $orderDetailHandle = fopen("/tmp/orderdetail.csv","w");
        foreach($orderid as $oid) {
            if(!empty($oid)) {
                //for order
                $result = $client->salesOrderInfo($session, $oid[0]);
                $result = (array)$result;
                $order_detail['order_id'] = $result['increment_id'];
                $order_detail['create_time'] = strtotime($result['created_at']);
                $order_detail['email'] = $result['customer_email'];
                $order_detail['shipping_fee'] = $result['shipping_amount'];
                $order_detail['total_money'] = $result['grand_total'];
                $order_detail['ip'] = sprintf("%u", ip2long($result['remote_ip']));  //ip需要转换为数字
                $order_detail['status'] = $result['status'];
                $order_detail['update_time'] = strtotime($result['updated_at']);
                $paymethod = (array)$result['payment'];
                $order_detail['pay_type'] = $paymethod['method'];
                $userinfo = (array)$result['shipping_address'];
                $order_detail['first_name'] = $userinfo['firstname'];
                $order_detail['last_name'] = $userinfo['lastname'];
                $order_detail['phone'] = $userinfo['telephone'];
                $order_detail['zip_code'] = $userinfo['postcode'];
                $order_detail['abbreviation'] = $userinfo['country_id'];
                $order_detail['country'] = $countrys[$userinfo['country_id']];  //国家全称
                $order_detail['state'] = $userinfo['region'];
                $order_detail['city'] = $userinfo['city'];
                $order_detail['address1'] = $userinfo['street'].','.$userinfo['city'].','.$userinfo['region'].','.$userinfo['postcode'].','.$order_detail['country'];
                foreach($userinfo['status_history'] as $paytypes) {
                    $paytypes = (array)$paytypes;
                    foreach($paytypes as $paytype) {
                        if($paytype['status'] == 'processing') {
                            $order_detail['pay_time'] = strtotime($paytype['created_at']); //支付时间
                            break;
                        }
                    }
                }

                //for orderdetail
                foreach($result['items'] as $key => $product) {
                    $product = (array)$product;
                    $product_item[$key]['product_id'] = $product['product_id'];
                    $product_item[$key]['order_id'] = $product['order_id'];
                    $product_item[$key]['sku_id'] = 0;
                    $product_item[$key]['uniqid'] = $product['sku'];
                    $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                    $product_item[$key]['total_money'] = $product['price']*$qty;
                    $product_item[$key]['pay_money'] = $product['price']*$qty;
                    $product_item[$key]['original_price'] = $product['original_price'];
                    $product_item[$key]['price'] = $product['price'];
                    $product_item[$key]['quantity'] = $qty;
                    $product_item[$key]['product_name'] = $product['name'];
                    $product_item[$key]['product_sku'] = $product['sku'];
                }
                $order_detail['items'] = serialize($product_item);

                fputcsv($orderDetailHandle,$order_detail);
            }
        }
        fclose($orderDetailHandle);*/
        echo 4;
    }

    /*
     * 方法说明：调用irulu的api进行用户数据的注册
     */
    public function uregisterAction() {

        /*$header = array(
            'X-App-Device: 10',
            'X-App-Version: 1.2.2',
            'X-Api-Version: v2.0',
            'NO-AUTH: 1',
            'X-UTM: ',
            'X-Uid: 0',
            'X-Did: sdfsfese',
            'X-Token: 55af66b41291512',
            //'X-FORWARDED-FOR: '. $ip,
            //'REMOTE-ADDR: '. $ip,
            //'CLIENT-IP: '. $ip
        );

        $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

        //$url_user = "http://api.account.irulu.com/register2/index?token=55af66b41291512";
        $url_user ="http://112.74.26.100:10083/register2/index?token=55af66b41291512";
        $url_order = "http://112.74.26.100:10083/order2/createOrder?token=55af66b41291512";

        $handle = fopen("/tmp/userinfo.csv","r");
        while(!feof($handle)) {
            $users_info[] = fgetcsv($handle);
        }
        $registerUserHandle = fopen("/tmp/register_user.csv","a+");
        foreach($users_info as $uk => $user_info) {
            if(!empty($user_info)) {
                $str_pos = strpos($user_info[3],":");
                $pwd = substr($user_info[3],0,$str_pos);
                $salt = substr($user_info[3],$str_pos+1);

                $user_data =  array(
                    'email'             => $user_info[0],
                    'lastname'            => $user_info[1],
                    'firstname'                => $user_info[2],
                    'password'             => '123456789',//$pwd,//$user_info[3],
                    'salt'             => 't456',//$salt,//$user_info[3],
                    'register_date'                 => $user_info[4],
                    'last_login_date'              => 1,
                    'user_type'           => '3',
                    'sex'         => 'F',  // 男：M   女：F
                    'post_addr'                => $user_info[5], //邮寄地址
                    'channel'              => 'pcweb',
                    'login_type'              => '1', //用户登录类型
                    'phone'              => $user_info[6],
                );

                //调用irulu的api
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url_user);  //1、url
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                curl_setopt($ch, CURLOPT_POST, 1);

                curl_setopt($ch, CURLOPT_POSTFIELDS, $user_data); //2、data
                $res = curl_exec($ch);

                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                $register_info[$uk]['uid'] = $res;
                $register_info[$uk]['email'] = $user_info[0];
                fputcsv($registerUserHandle,$register_info[$uk]);
            }
        }
        fclose($registerUserHandle);
        fclose($handle);*/
        echo 5;
    }


    /*
     * 方法说明：获取magento订单并通过irulu的api保存到数据库
     */
    public function oaddAction() {
        /*$header = array(
            'X-App-Device: 10',
            'X-App-Version: 1.2.2',
            'X-Api-Version: v2.0',
            'NO-AUTH: 1',
            'X-UTM: ',
            'X-Uid: 0',
            'X-Did: sdfsfese',
            'X-Token: 55af66b41291512',
            //'X-FORWARDED-FOR: '. $ip,
            //'REMOTE-ADDR: '. $ip,
            //'CLIENT-IP: '. $ip
        );

        $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

        //$url_user = "http://api.account.irulu.com/register2/index?token=55af66b41291512";
        $url_order = "http://112.74.26.100:10084/order2/createOrder?token=55af66b41291512";
        $url_order_detail = "http://112.74.26.100:10084/order2/insertOrderDetail?token=55af66b41291512";
        $url_getuid ="http://112.74.26.100:10083/register2/getuid?token=55af66b41291512";

        $productHandle = fopen("/tmp/product.csv","a+");
        $orderDetailHandle = fopen("/tmp/orderdetail.csv","r");
        $orderLogHandle = fopen("/tmp/order_log.csv","a+");
        while(!feof($orderDetailHandle)) {
            $orders_info[] = fgetcsv($orderDetailHandle);
        }

        foreach($orders_info as $ok => $order_info) {
            if(!empty($order_info)) {
                //根据email获取用户的uid
                $email = array('email' => $order_info[2]);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url_getuid);  //1、url
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                curl_setopt($ch, CURLOPT_POST, 1);

                curl_setopt($ch, CURLOPT_POSTFIELDS, $email); //2、data
                $uid = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);


                $data_order = array(
                    'order_id' => $order_info[0],
                    'uid' => $uid,
                    'mid' => 1,
                    'total_money' => $order_info[4],  //这里需要在购物的时候加个购物券测试是否是折扣后的价格
                    'pay_money' => $order_info[4],
                    'receive_money' => $order_info[4],
                    'shipping_fee' => $order_info[3],
                    'savings_fee' => 0,
                    'insurance_fee' => 0, //商品物流保险费用
                    'first_name' => $order_info[9],
                    'last_name' => $order_info[10],
                    'phone' => $order_info[11],
                    'email' => $order_info[2],
                    'zip_code' => $order_info[12],
                    'abbreviation' => $order_info[13], //国家缩写
                    'country' => $order_info[14],
                    'state' => $order_info[15],
                    'city' => $order_info[16],
                    'address1' => $order_info[17],
                    'address2' => '',
                    'status' => 1,
                    'device_id' => 'pcweb',
                    'platform' => 0,
                    'order_source' => 10,
                    'channel' => "pcweb",
                    'ip' => "1365063955",//$order_info[5],
                    'pay_time' => time(),
                    'create_time' => $order_info[1],
                    'update_time' => $order_info[7],
                    'expired_time' => 0,
                    'pay_type' => 1//$order_info[8]  //1：Paypal、2：信用卡'  支付方式这里需要改为数字
                );

                //调用irulu的api
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url_order);  //1、url
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                curl_setopt($ch, CURLOPT_POST, 1);

                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_order); //2、data
                $res = curl_exec($ch);

                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                $order_log[$ok]['oid'] = $res;
                $order_log[$ok]['email'] = $order_info[2];
                fputcsv($orderLogHandle, $order_log[$ok]);

                $order_products = unserialize($order_info[18]);
                foreach ($order_products as $key => $product) {
                    if (!empty($product)) {
                        $order_detail_data = array(    //一个订单中有多个产品的时候，需要多个这样的数据结构
                            'order_id' => $data_order['order_id'],
                            'uid' => $uid,  //这里需要填写uid
                            'parent_id' => 0,
                            'product_id' => $product['product_id'],
                            'sku_id' => 0,
                            // 'total_money'           => $product['originalPrice'] * $product['quantity'],
                            'total_money' => $product['total_money'],
                            'pay_money' => $product['pay_money'],
                            'original_price' => $product['original_price'],
                            'price' => $product['price'],
                            'quantity' => $product['quantity'],
                            'product_name' => $product['product_name'],
                            'product_sku' => '',
                            'product_image' => '',
                            'uniqid' => $product['product_sku'],  //sku
                            'promotion_product_id' => 0
                        );

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url_order_detail);  //1、url
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                        curl_setopt($ch, CURLOPT_POST, 1);

                        curl_setopt($ch, CURLOPT_POSTFIELDS, $order_detail_data); //2、data
                        $res = curl_exec($ch);

                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);
                        $order_log[$ok]['did'] = $res;
                        $order_log[$ok]['email'] = $order_info[2];
                        $order_log[$ok]['uid'] = $uid;
                        fputcsv($productHandle, $order_log[$ok]);
                    }
                }
            }
        }
        fclose($productHandle);
        fclose($orderDetailHandle);
        fclose($orderLogHandle);*/
        echo 6;
    }

/*    public function getuidAction() {
        $url_user ="http://112.74.26.100:10083/register2/getuid?token=55af66b41291512";
        $header = array(
            'X-App-Device: 10',
            'X-App-Version: 1.2.2',
            'X-Api-Version: v2.0',
            'NO-AUTH: 1',
            'X-UTM: ',
            'X-Uid: 0',
            'X-Did: sdfsfese',
            'X-Token: 55af66b41291512',
            //'X-FORWARDED-FOR: '. $ip,
            //'REMOTE-ADDR: '. $ip,
            //'CLIENT-IP: '. $ip
        );

        $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

        $email = array('email' => "cren3@163.com");;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url_user);  //1、url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $email); //2、data
        $res = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        echo "<pre>";
        print_r($res);
    }*/
}