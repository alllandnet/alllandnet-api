<?php
/**
 * Created by PhpStorm.
 * User: jacki_hu
 * Date: 2017/5/12
 * Time: 22:08
 */

class Helloworld_Wyd_IndexController extends Mage_Core_Controller_Front_Action
{

    /*
     * 方法说明：获取用户列表
     * 调用方法：http://domain/index.php/account/index/userlist
     */
    public function userlistAction() {
        if(file_exists("/tmp/user_list.csv")) {
            unlink("/tmp/user_list.csv");
        }
        $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $session = $client->login('irulu', 'iruluapitest');

        //当前执行脚本的时间
        //$current_time = 1497412800;  //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400;//time();  //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400+86400;  //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400+86400+86400; //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400; //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400+86400;  //20170619  //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400+86400+86400; //20170620 12:01:11 这个时间的用户已经导入，没有付款订单
        $current_time = 1497412800+86400+86400+86400+86400+86400+86400+86400;  //这个时间的用户和订单都已经导入
        $current_time_date = date("Y-m-d H:i:s",$current_time);
        //获取24小时前的数据
        $yesterday_time = $current_time - 86400;  //这个时间作为查询时间，以当前时间为基准向前推24小时
        $yesterday_time_date = date("Y-m-d H:i:s",$yesterday_time);

        $complexFilter = array(
            'complex_filter' => array(
                array(
                    'key' => 'created_at',
                    'value' => array('key' => 'gt', 'value' => $yesterday_time_date)  //筛选注册时间大于某个时间点的用户列表
                )
            )
        );

        $user_list = $client->customerCustomerList($session, $complexFilter); //获取用户列表

        foreach($user_list as $key => $user) {
            $user = (array)$user;
            $order_time = strtotime($user['created_at']);
            if(($order_time <= $current_time) && ($order_time > $yesterday_time)) {  //获取24小时内的注册用户数据
                continue;
            } else {
                unset($user_list[$key]);
            }
        }

        //这里的每个方法都需要判断一下从数据库中是否有获取到数据，如果有才创建文件，如果没有就不要创建，并且不要继续往下执行用户注册，但是订单与用户注册要分开
        //将用户列表写入文件中，因为magento的api获取时间交长，为了防止在操作过程中连接中断造成错误
        $handle = fopen("/tmp/user_list.csv","w");
        foreach($user_list as $user) {
            $user = (array)$user;
            $user['created_at'] = strtotime($user['created_at']);
            $user['updated_at'] = strtotime($user['updated_at']);
            if(isset($user['middlename'])) {
                unset($user['middlename']);
            }
            fputcsv($handle,$user);
        }
        fclose($handle);

        $usersHandle = fopen("/tmp/users.txt","w");
        fwrite($usersHandle,var_export($user_list,true));
        fclose($usersHandle);

        echo 1;
    }

    /*
     * 方法说明：用户地址获取
     * 调用方法：http://domain/index.php/account/index/useraddress
     */
    public function useraddressAction() {
        if(file_exists("/tmp/userinfo.csv")) {
            unlink("/tmp/userinfo.csv");
        }
        if(file_exists("/tmp/user_list.csv")) {
            $client = new SoapClient('https://www.irulu.com/index.php/api/soap/?wsdl');
            $session = $client->login('irulu', 'iruluapitest');

            $handle = fopen("/tmp/user_list.csv","r");

            while(!feof($handle)) {
                $content[] = fgetcsv($handle);
            }
            fclose($handle);

            if(!empty($content)) {
                $userHandle = fopen("/tmp/userinfo.csv","w");

                foreach($content as $k => $user) {
                    if(!empty($user)) {
                        $userinfo[$k]['email'] = $user[6];
                        $userinfo[$k]['lastname'] = $user[8];
                        $userinfo[$k]['firstname'] = $user[7];
                        $userinfo[$k]['password'] = $user[10];
                        $userinfo[$k]['register_date'] = $user[1];
                        $result = $client->call($session, 'customer_address.list', $user[0]); //获取用户地址列表
                        $userinfo[$k]['post_addr'] = $result[0]['street'].','.$result[0]['city'].','.$result[0]['region'].','.$result[0]['postcode'].','.$result[0]['country_id'];
                        $userinfo[$k]['phone'] = $result[0]['telephone'];

                        //将$userinfo数据写入到csv文件中，然后再通过另一个方法读取该文件并将数据通过调用旧网站的api写入旧的数据库
                        fputcsv($userHandle,$userinfo[$k]);
                    }
                }
                fclose($userHandle);
                unlink("/tmp/user_list.csv");
                echo 1;
            }
        }
    }

    /*
     * 方法说明：获取所有订单列表
     *调用方法：http://domain/index.php/account/index/orderlist
     */
    public function orderlistAction() {
        if(file_exists("/tmp/orderid.csv")) {
            unlink("/tmp/orderid.csv");
        }
        $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $session = $client->login('irulu', 'iruluapitest');

        //当前执行脚本的时间
        //$current_time = 1497412800; //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400;//time();  //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400+86400;  //这个时间的用户已经导入，没有付款订单
        //$current_time = 1497412800+86400+86400+86400;  //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400;  //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400+86400;  //20170619  //这个时间的用户和订单已经导入
        //$current_time = 1497412800+86400+86400+86400+86400+86400+86400;  //20170620 12:01:11  这个时间的用户已经导入，没有付款订单
        $current_time = 1497412800+86400+86400+86400+86400+86400+86400+86400;  //这个时间的用户和订单都已经导入
        $current_time_date = date("Y-m-d H:i:s",$current_time);
        //获取24小时前的数据
        $yesterday_time = $current_time - 86400;  //这个时间作为查询时间，以当前时间为基准向前推24小时
        $yesterday_time_date = date("Y-m-d H:i:s",$yesterday_time);


        $filter = array('complex_filter' => array(array('key' => 'created_at', 'value' => array('key' => 'gt','value'=>$yesterday_time_date))));
        $order_list = $client->salesOrderList($session,$filter);

        if(!empty($order_list)) {
            $orderidHandle = fopen("/tmp/orderid.csv","w");
            $orderSumHandle = fopen("/tmp/order_sum.csv","a+");

            //先通过magento的API筛选出最近24小时的订单，然后再判断订单中已经付款的留下，没有付款的删除
            //在这里筛选
            $orderHandle = fopen("/tmp/orders.txt","w");

            foreach($order_list as $k => $od) {
                $od = (array)$od;
                $order_time = strtotime($od['created_at']);
                if(($order_time <= $current_time) && ($order_time > $yesterday_time)) {
                    continue;
                } else {
                    unset($order_list[$k]);
                }
            }

            foreach($order_list as $key => $order) {
                $order = (array)$order;
                if($order['status'] == 'processing') {
                    continue;
                } else {
                    unset($order_list[$key]);
                }
            }

            fwrite($orderHandle,var_export($order_list,true));


            foreach($order_list as $order) {
                $order = (array)$order;
                $orderid[0]=$order['increment_id'];

                fputcsv($orderidHandle,$orderid);
                fputcsv($orderSumHandle,$orderid);
            }
            fclose($orderidHandle);
            fclose($orderSumHandle);
            fclose($orderHandle);
            echo 1;
        }
    }

    /*
     * 方法说明：获取订单详情
     */
    public function orderdetailAction() {
        if(file_exists("/tmp/orderdetail.csv")) {
            unlink("/tmp/orderdetail.csv");
        }
        $client = new SoapClient('https://www.irulu.com/index.php/api/v2_soap/?wsdl');
        $session = $client->login('irulu', 'iruluapitest');

        $countrys = array (
            'AE' => 'UNITED EMIRATES',
            'AF' => 'AFGHANISTAN',
            'AL' => 'ALBANIA',
            'AM' => 'ARMENIA',
            'AO' => 'ANGOLA',
            'AR' => 'ARGENTINA',
            'AT' => 'AUSTRIA',
            'AU' => 'AUSTRALIA',
            'AZ' => 'AZERBAIJAN(REPUBLIC',
            'BD' => 'BANGLADESH',
            'BE' => 'BELGIUM',
            'BF' => 'BURKINA FASO',
            'BG' => 'BULGARIA',
            'BH' => 'BAHREIN',
            'BI' => 'BURUNDI',
            'BJ' => 'BENIN',
            'BN' => 'BRUNEI DARUSSALAM',
            'BO' => 'BOLIVIA',
            'BR' => 'BRAZIL',
            'BW' => 'BOTSWANA',
            'BY' => 'BYELORUSSIA',
            'CA' => 'CANADA',
            'CF' => 'CENTRAL AFRICA',
            'CG' => 'CONGO',
            'CH' => 'SWITZERLAND',
            'CL' => 'CHILE',
            'CM' => 'CAMEROON',
            'CN' => 'CHINA',
            'CO' => 'COLOMBIA',
            'CR' => 'COSTA RICA',
            'CS' => 'CZECH REPUBIC',
            'CU' => 'CUBA',
            'CY' => 'CYPRUS',
            'DE' => 'GERMANY',
            'DK' => 'DENMARK',
            'DO' => 'DOMINICAN REPUBLIC',
            'DZ' => 'ALGERIA',
            'EC' => 'ECUADOR',
            'EE' => 'ESTONIA',
            'EG' => 'EGYPT',
            'ES' => 'SPAIN',
            'ET' => 'ETHIOPIA',
            'FI' => 'FINLAND',
            'FJ' => 'FIJI',
            'FR' => 'FRANCE',
            'GA' => 'GABON',
            'GB' => 'UNITED KINGDOM',
            'GD' => 'GRENADA',
            'GE' => 'GEORGIA',
            'GH' => 'GHANA',
            'GN' => 'GUINEA',
            'GR' => 'GREECE',
            'GT' => 'GUATEMALA',
            'HK' => 'HONG KONG',
            'HN' => 'HONDURAS',
            'HU' => 'HUNGARY',
            'ID' => 'INDONESIA',
            'IE' => 'IRELAND',
            'IL' => 'ISRAEL',
            'IN' => 'INDIA',
            'IQ' => 'IRAQ',
            'IR' => 'IRAN',
            'IS' => 'ICELAND',
            'IT' => 'ITALY',
            'JM' => 'JAMAICA',
            'JO' => 'JORDAN',
            'JP' => 'JAPAN',
            'KG' => 'KYRGYZSTAN',
            'KH' => 'KAMPUCHEA(CAMBODIA',
            'KP' => 'KOREA,DEM.PEOPLE\'S',
            'KR' => 'REPUBLIC OF KOREA',
            'KT' => 'COTE O\'IVOIRE',
            'KW' => 'KUWATI',
            'KZ' => 'KAZAKHSTAN',
            'LA' => 'LAOS',
            'LB' => 'LEBANON',
            'LC' => 'SAINT LUEIA',
            'LI' => 'LIECHTENSTEIN',
            'LK' => 'SRI LANKA',
            'LR' => 'LIBERIA',
            'LT' => 'LITHUANIA',
            'LU' => 'LUXEMBOURG',
            'LV' => 'LATVIA',
            'LY' => 'LIBYAN',
            'MA' => 'MOROCCO',
            'MC' => 'MONACO',
            'MD' => 'MOLDOVA,REPUBLIC OF',
            'MG' => 'MADAGASCAR',
            'ML' => 'MALI',
            'MM' => 'BURMA(MYANMAR',
            'MN' => 'MONGOLIA',
            'MO' => 'MACAU',
            'MT' => 'MALTA',
            'MU' => 'MAURITIUS',
            'MW' => 'MALAWI',
            'MX' => 'MEXICO',
            'MY' => 'MALAYSIA',
            'MZ' => 'MOZAMBIQUE',
            'NA' => 'NAMIBIA',
            'NE' => 'NIGER',
            'NG' => 'NIGERIA',
            'NI' => 'NICARAGUA',
            'NL' => 'NETHERLANDS',
            'NO' => 'NORWAY',
            'NP' => 'NEPAL',
            'NZ' => 'NEW ZEALAND',
            'OM' => 'OMAN',
            'PA' => 'PANAMA',
            'PE' => 'PERU',
            'PG' => 'PAPUA NEW GUINEA',
            'PH' => 'PHILIPPINES',
            'PK' => 'PAKISTAN',
            'PL' => 'POLAND',
            'PT' => 'PORTUGAL',
            'PY' => 'PARAGUAY',
            'QA' => 'QATAR',
            'RO' => 'ROMANIA',
            'RU' => 'RUSSIAN FEDERATION',
            'SA' => 'SAUDI ARABIA',
            'SC' => 'SEYCHELLES',
            'SD' => 'SUDAN',
            'SE' => 'SWEDEN',
            'SG' => 'SINGAPORE',
            'SI' => 'SLOVENIA',
            'SK' => 'SLOVAKIA',
            'SM' => 'SAN MARINO',
            'SN' => 'SENEGAL',
            'SO' => 'SOMALIA',
            'SY' => 'SYRIA',
            'SZ' => 'SWAZILAND',
            'TD' => 'CHAD',
            'TG' => 'TOGO',
            'TH' => 'THAILAND',
            'TJ' => 'TAJIKISTAN',
            'TM' => 'TURKMENISTAN',
            'TN' => 'TUNISIA',
            'TR' => 'TURKEY',
            'TW' => 'TAIWAN',
            'TZ' => 'TANZANIA',
            'UA' => 'UKRAINE',
            'UG' => 'UGANDA',
            'US' => 'UNITED STATES',
            'UY' => 'URUGUAY',
            'UZ' => 'UZBEKISTAN',
            'VC' => 'SAINT VINCENT',
            'VE' => 'VENEZUELA',
            'VN' => 'VIET NAM',
            'YE' => 'YEMEN',
            'YU' => 'YUGOSLAVIA',
            'ZA' => 'SOUTH AFRICA',
            'ZM' => 'ZAMBIA',
            'ZR' => 'ZAIRE',
            'ZW' => 'ZIMBABWE',
        );

        $orderidHandle = fopen("/tmp/orderid.csv","r");
        while(!feof($orderidHandle)) {
            $orderid[] = fgetcsv($orderidHandle);
        }
        fclose($orderidHandle);

        $idHandle = fopen("/tmp/order_id_txt.txt","w");
        fwrite($idHandle,var_export($orderid,true));
        fclose($idHandle);

        if(isset($orderid) && !empty($orderid[0])) {
            $orderDetailHandle = fopen("/tmp/orderdetail.csv","w");
            $orderDetailLogHandle = fopen("/tmp/order_detail_log.txt","w");
            foreach($orderid as $oid) {
                if(!empty($oid)) {
                    //for order
                    $result = $client->salesOrderInfo($session, $oid[0]);
                    $result = (array)$result;
                    $order_detail['order_id'] = $result['increment_id'];
                    $order_detail['create_time'] = strtotime($result['created_at']);
                    $order_detail['email'] = $result['customer_email'];
                    $order_detail['shipping_fee'] = $result['shipping_amount'];
                    $order_detail['total_money'] = $result['grand_total'];
                    $order_detail['ip'] = !isset($result['remote_ip']) ? sprintf("%u", ip2long($result['remote_ip'])) : "1883904612";  //ip需要转换为数字
                    $order_detail['status'] = $result['status'];
                    $order_detail['update_time'] = strtotime($result['updated_at']);
                    $paymethod = (array)$result['payment'];
                    $order_detail['pay_type'] = $paymethod['method'];
                    $userinfo = (array)$result['shipping_address'];
                    $order_detail['first_name'] = $userinfo['firstname'];
                    $order_detail['last_name'] = $userinfo['lastname'];
                    $order_detail['phone'] = $userinfo['telephone'];
                    $order_detail['zip_code'] = $userinfo['postcode'];
                    $order_detail['abbreviation'] = $userinfo['country_id'];
                    $order_detail['country'] = $countrys[$userinfo['country_id']];  //国家全称
                    $order_detail['state'] = $userinfo['region'];
                    $order_detail['city'] = $userinfo['city'];
                    $order_detail['address1'] = $userinfo['street'].','.$userinfo['city'].','.$userinfo['region'].','.$userinfo['postcode'].','.$order_detail['country'];
                    foreach($userinfo['status_history'] as $paytypes) {
                        $paytypes = (array)$paytypes;
                        foreach($paytypes as $paytype) {
                            if($paytype['status'] == 'processing') {
                                $order_detail['pay_time'] = strtotime($paytype['created_at']); //支付时间
                                break;
                            }
                        }
                    }

                    //for orderdetail
                    foreach($result['items'] as $key => $product) {
                        $product = (array)$product;
                        $product_item[$key]['product_id'] = $product['product_id'];
                        $product_item[$key]['order_id'] = $product['order_id'];
                        $product_item[$key]['sku_id'] = 0;
                        $product_item[$key]['uniqid'] = $product['sku'];
                        $qty = ceil($product['qty_ordered']) - ceil($product['qty_canceled']);
                        $product_item[$key]['total_money'] = $product['price']*$qty;
                        $product_item[$key]['pay_money'] = $product['price']*$qty;
                        $product_item[$key]['original_price'] = $product['original_price'];
                        $product_item[$key]['price'] = $product['price'];
                        $product_item[$key]['quantity'] = $qty;
                        $product_item[$key]['product_name'] = $product['name'];
                        $product_item[$key]['product_sku'] = $product['sku'];
                        $product_item[$key]['product_type'] = $product['product_type'];
                    }
                    $order_detail['items'] = serialize($product_item);

                    fputcsv($orderDetailHandle,$order_detail);
                }/* else {
                $order_detail['items'] = "empty";
                $order_detail['oid'] = $oid[0];

                fputcsv($orderDetailHandle,$order_detail);
            }*/

                //write log
                fwrite($orderDetailLogHandle,$oid);
                fwrite($orderDetailLogHandle,"\n");
                fwrite($orderDetailLogHandle,var_export($order_detail,true));
            }
            fclose($orderDetailHandle);
            fclose($orderDetailLogHandle);
            unlink("/tmp/orderid.csv");
            echo 1;
        }
    }

    /*
     * 方法说明：调用irulu的api进行用户数据的注册
     */
    public function uregisterAction() {

        $header = array(
            'X-App-Device: 10',
            'X-App-Version: 1.2.2',
            'X-Api-Version: v2.0',
            'NO-AUTH: 1',
            'X-UTM: ',
            'X-Uid: 0',
            'X-Did: sdfsfese',
            'X-Token: 55af66b41291512',
            //'X-FORWARDED-FOR: '. $ip,
            //'REMOTE-ADDR: '. $ip,
            //'CLIENT-IP: '. $ip
        );

        $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

        $url_user = "http://47.88.4.2:8082/register2/index?token=55af66b41291512";
        //$url_user ="http://api.account.irulu.com/register2/index?token=55af66b41291512";
        //$url_user ="http://112.74.26.100:10083/register2/index?token=55af66b41291512";
        //$url_order = "http://112.74.26.100:10083/order2/createOrder?token=55af66b41291512";

        $handle = fopen("/tmp/userinfo.csv","r");
        while(!feof($handle)) {
            $users_info[] = fgetcsv($handle);
        }

        $useriHandle = fopen("/tmp/user_info_txt.txt","w");
        fwrite($useriHandle,var_export($users_info,true));
        fclose($useriHandle);

        if(isset($users_info) && !empty($users_info[0])) {
            $registerUserHandle = fopen("/tmp/register_user.csv","a+");
            foreach($users_info as $uk => $user_info) {
                if(!empty($user_info)) {
                    $str_pos = strpos($user_info[3],":");
                    $pwd = substr($user_info[3],0,$str_pos);
                    $salt = substr($user_info[3],$str_pos+1);

                    $user_data =  array(
                        'email'             => $user_info[0],
                        'lastname'            => $user_info[1],
                        'firstname'                => $user_info[2],
                        'password'             => $pwd,//$user_info[3],
                        'salt'             => $salt,//$user_info[3],
                        'register_date'                 => $user_info[4],
                        'last_login_date'              => 1,
                        'user_type'           => '3',
                        'sex'         => 'F',  // 男：M   女：F
                        'post_addr'                => $user_info[5], //邮寄地址
                        'channel'              => 'pcweb',
                        'login_type'              => '1', //用户登录类型
                        'phone'              => $user_info[6],
                    );

                    //调用irulu的api
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url_user);  //1、url
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                    curl_setopt($ch, CURLOPT_POST, 1);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $user_data); //2、data
                    $res = curl_exec($ch);

                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    $register_info[$uk]['uid'] = $res;
                    $register_info[$uk]['email'] = $user_info[0];
                    fputcsv($registerUserHandle,$register_info[$uk]);
                }
            }
            fclose($registerUserHandle);
            fclose($handle);
            unlink("/tmp/userinfo.csv");
            echo 1;
        }
    }


    /*
     * 方法说明：获取magento订单并通过irulu的api保存到数据库
     */
    public function oaddAction() {
        $header = array(
            'X-App-Device: 10',
            'X-App-Version: 1.2.2',
            'X-Api-Version: v2.0',
            'NO-AUTH: 1',
            'X-UTM: ',
            'X-Uid: 0',
            'X-Did: sdfsfese',
            'X-Token: 55af66b41291512',
            //'X-FORWARDED-FOR: '. $ip,
            //'REMOTE-ADDR: '. $ip,
            //'CLIENT-IP: '. $ip
        );

        $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

        //$url_user = "http://api.account.irulu.com/register2/index?token=55af66b41291512";
        $url_order = "http://47.88.4.2/order2/createOrder?token=55af66b41291512";
        $url_order_detail = "http://47.88.4.2/order2/insertOrderDetail?token=55af66b41291512";
        $url_getuid ="http://47.88.4.2:8082/register2/getuid?token=55af66b41291512";

        /*        $url_order = "http://112.74.26.100:10084/order2/createOrder?token=55af66b41291512";
                $url_order_detail = "http://112.74.26.100:10084/order2/insertOrderDetail?token=55af66b41291512";
                $url_getuid ="http://112.74.26.100:10083/register2/getuid?token=55af66b41291512";*/

        $productHandle = fopen("/tmp/product.txt","a+");
        $orderDetailHandle = fopen("/tmp/orderdetail.csv","r");
        $orderLogHandle = fopen("/tmp/order_log.txt","a+");
        $showHandle = fopen("/tmp/show_log.txt","w");
        $uidHandle = fopen("/tmp/uid.csv","w");
        while(!feof($orderDetailHandle)) {
            $orders_info[] = fgetcsv($orderDetailHandle);
        }

        if(isset($orders_info) && !empty($orders_info[0])) {
            foreach($orders_info as $ok => $order_info) {
                if(!empty($order_info)) {
                    //根据email获取用户的uid
                    $email = array('email' => $order_info[2]);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url_getuid);  //1、url
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                    curl_setopt($ch, CURLOPT_POST, 1);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $email); //2、data
                    $uid = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    $u[0] = $uid;
                    fputcsv($uidHandle,$u);
                    $data_order = array(
                        'order_id' => $order_info[0],
                        'uid' => $uid,
                        'mid' => 1,
                        'total_money' => $order_info[4],  //这里需要在购物的时候加个购物券测试是否是折扣后的价格
                        'pay_money' => $order_info[4],
                        'receive_money' => $order_info[4],
                        'shipping_fee' => $order_info[3],
                        'savings_fee' => 0,
                        'insurance_fee' => 0, //商品物流保险费用
                        'first_name' => $order_info[9],
                        'last_name' => $order_info[10],
                        'phone' => $order_info[11],
                        'email' => $order_info[2],
                        'zip_code' => $order_info[12],
                        'abbreviation' => $order_info[13], //国家缩写
                        'country' => $order_info[14],
                        'state' => $order_info[15],
                        'city' => $order_info[16],
                        'address1' => $order_info[17],
                        'address2' => '',
                        'status' => 3, //1 未付款；3 已付款
                        'device_id' => 'pcweb',
                        'platform' => 0,
                        'order_source' => 10,
                        'channel' => "pcweb",
                        'ip' => !empty($order_info[5]) ? $order_info[5] : "1883904612",  //ip需要转换为数字,
                        'pay_time' => time(),
                        'create_time' => $order_info[1],
                        'update_time' => $order_info[7],
                        'expired_time' => 0,
                        'pay_type' => 1//$order_info[8]  //1：Paypal、2：信用卡'  支付方式这里需要改为数字
                    );
                    fwrite($showHandle,var_export($data_order,true));
                    //调用irulu的api
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url_order);  //1、url
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                    curl_setopt($ch, CURLOPT_POST, 1);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_order); //2、data
                    $res = curl_exec($ch);

                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    $order_log[$ok]['oid'] = $res;
                    $order_log[$ok]['email'] = $order_info[2];
                    $order_log[$ok]['mess'] = $uid;
                    //fputcsv($orderLogHandle, $order_log[$ok]);
                    fwrite($orderLogHandle,var_export($order_log[$ok],true));
                    fwrite($orderLogHandle,"\n");
                    $order_products = unserialize($order_info[18]);
                    foreach ($order_products as $key => $product) {
                        if (!empty($product)) {
                            $order_detail_data = array(    //一个订单中有多个产品的时候，需要多个这样的数据结构
                                'order_id' => $data_order['order_id'],
                                'uid' => $uid,  //这里需要填写uid
                                'parent_id' => 0,
                                'product_id' => $product['product_id'],
                                'sku_id' => 0,
                                // 'total_money'           => $product['originalPrice'] * $product['quantity'],
                                'total_money' => $product['total_money'],
                                'pay_money' => $product['pay_money'],
                                'original_price' => $product['original_price'],
                                'price' => $product['price'],
                                'quantity' => $product['quantity'],
                                'product_name' => $product['product_name'],
                                'product_sku' => '',
                                'product_image' => '',
                                'uniqid' => $product['product_sku'],  //sku
                                'promotion_product_id' => 0
                            );

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url_order_detail);  //1、url
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                            curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

                            curl_setopt($ch, CURLOPT_POST, 1);

                            curl_setopt($ch, CURLOPT_POSTFIELDS, $order_detail_data); //2、data
                            $res = curl_exec($ch);

                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            curl_close($ch);
                            $order_log[$ok]['did'] = $res;
                            $order_log[$ok]['email'] = $order_info[2];
                            $order_log[$ok]['product'] = $order_detail_data;
                            $order_log[$ok]['uid'] = $uid;
                            //fputcsv($productHandle, $order_log[$ok]);
                            fwrite($productHandle,var_export($order_log[$ok],true));
                            fwrite($productHandle,"\n");
                        }
                    }
                }/* else {
                    $order_log[$ok]['did'] = $res;
                    $order_log[$ok]['order'] = $data_order['order_id'];
                    $order_log[$ok]['res'] = "empty";
                    fwrite($productHandle,var_export($order_log[$ok]));
                    fwrite($productHandle,"\n");
                }*/
            }
            fclose($productHandle);
            fclose($orderDetailHandle);
            fclose($orderLogHandle);
            fclose($showHandle);
            fclose($uidHandle);
            unlink("/tmp/orderdetail.csv");
            echo 1;
        }
    }

    /*    public function getuidAction() {
            $url_user ="http://112.74.26.100:10083/register2/getuid?token=55af66b41291512";
            $header = array(
                'X-App-Device: 10',
                'X-App-Version: 1.2.2',
                'X-Api-Version: v2.0',
                'NO-AUTH: 1',
                'X-UTM: ',
                'X-Uid: 0',
                'X-Did: sdfsfese',
                'X-Token: 55af66b41291512',
                //'X-FORWARDED-FOR: '. $ip,
                //'REMOTE-ADDR: '. $ip,
                //'CLIENT-IP: '. $ip
            );

            $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

            $email = array('email' => "cren3@163.com");;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url_user);  //1、url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

            curl_setopt($ch, CURLOPT_POST, 1);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $email); //2、data
            $res = curl_exec($ch);

            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            echo "<pre>";
            print_r($res);
        }*/

    public function getproductAction() {
        $handle = fopen("/tmp/orderdetail.csv","r");
        while(!feof($handle)) {
            $orders[] = fgetcsv($handle);
        }
        fclose($handle);
        $file = fopen("/tmp/order_count.txt","w");
        $pro = fopen("/tmp/product_count,txt","w");
        fwrite($file,var_export($orders,true));

        foreach($orders as $order) {
            $products = unserialize($order[18]);
            fwrite($pro,var_export($products,true));
        }

        fclose($file);
        fclose($pro);
    }
}